### What is this repository for? ###

The repository contains design files for the Simplified ALPIDE Arduino Shield.
Simplified ALPIDE shield is a carrier board for the ALPIDE chip, a new sensor used for tracking particle collisions. The sensor are to be used on the upgraded detection module on the ALICE Experiment at CERN.
The purpose of this project is to reengineer an already existing carrier design as well as making it easier to use, as the project aims to produce board which could be used in an educational setting.
Originally based on the CERN ITS Teams ALPIDE Arduino Shield Design.
This design is redrawn for use in KiCad.

### How do I get set up? ###

Open the project file in KiCad and the project should load.
There are two libraries containing footprint + schematic symbols used in the schematic these are:
These needs to be downloaded to use the models!

https://github.com/mcous/kicad-lib/tree/master/footprints - SOP Footprint used

https://github.com/Alarm-Siren/arduino-kicad-library - Arduino Schematic Part Used, new footprint supplied with the repository, based on one of the footprint in this library (UNO Shield)

Credit to the original creators of these libraries.

The ALPIDE C library and ALPIDE GUI contains files for programming the ARDUINO as well as the visualization software used for the system
Credit goes to the CERN ITS team for developing the software as well as the circutry and the ALPIDE sensor which this project is based around.

### Who do I talk to? ###

For questions or other relevant inquiries
E-mail: benjamin@kebas.no