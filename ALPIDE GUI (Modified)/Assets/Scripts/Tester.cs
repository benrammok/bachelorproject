﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tester : MonoBehaviour {

//	// Use this for initialization
//	void Start () {
//		
//	}
	public float movespeed = 5.0f;
	public float rotatespeed = 30.0f;
	private Vector3 point1;
	private Vector3 point2;
	private Vector3 point3;

	// Update is called once per frame
	void Start () {

		point1 = new Vector3 (50, 0f, 30);
		point2 = new Vector3 (50, 12f, 30);
		point3 = new Vector3 (20, 24f, 150);


		Debug.Log (FindAngle(point1,point2,point3));

		point1 = new Vector3 (50, 0f, 30);
		point2 = new Vector3 (50, 0f, 30);
		point3 = new Vector3 (20, 0f, 150);


		Debug.Log (FindAngle(point1,point2,point3));
	}

	float FindAngle (Vector3 p1, Vector3 p2, Vector3 p3 )
	{// finds the best angle between the two vectors formed from 3 planes

		float x_pixel = 0.2924f;// pixel dimensions
		float z_pixel = 0.2688f;

		p1 =  new Vector3 (p1.x*x_pixel,p1.y, p1.z*z_pixel);
		p2 =  new Vector3 (p2.x*x_pixel,p2.y, p2.z*z_pixel);
		p3 =  new Vector3 (p3.x*x_pixel,p3.y, p3.z*z_pixel);

		Vector3 p21 = p1 - p2;
		Vector3 p23 = p3 - p2;

		//	Debug.Log (p21);
		//	Debug.Log (p23);
		//float mag1 = p21.magnitude;
		//	float mag2 = p23.magnitude;

		//	float dotpro = Vector3.Dot (p21, p23);

		//	float anglerad = Mathf.Acos (dotpro/(mag1*mag2));
		//	float angledeg = anglerad * (float)(180.0f / Math.PI);
		//	Debug.Log(Vector3.Dot(p21,p23));
		//Debug.Log(angledeg);

		return Vector3.Angle (p21, p23);
		//Debug.Log (ang);
		//return ang;





		//Debug.Log (transform.forward);
		//Vector3 directiontarget = pos3 - pos2;
		//		Vector3 targetDir = pos1 - pos2;
		//		Debug.Log (targetDir);
		//		float angle = Vector3.Angle(targetDir, new Vector3(1,0,0));
		//		Debug.Log (angle);
		//
		//		float height = 12.0f; // in mm
		//		float x_pixel = 0.2924f;
		//		float z_pixel = 0.2688f;
		//		float distxmm = (pos1.x-pos2.x)* (1/dividerx)*x_pixel;
		//		float distzmm = (pos1.z-pos2.z)* (1/dividerz)*z_pixel;
		//
		//		float distance = Mathf.Sqrt (Mathf.Pow (distxmm, 2.0f) + Mathf.Pow (distzmm, 2.0f));
		//		float anglerad = Mathf.Atan (height / distance);
		//		float angledeg = anglerad * (float)(180.0f / Math.PI);
		//		Debug.Log ("Angle: " + angledeg);
		//		Debug.Log ("XZ Distance: " + distance);
		//		Debug.Log ("X Distance: " + distxmm);
		//		Debug.Log ("Z Distance: " + distzmm);

	}

}
