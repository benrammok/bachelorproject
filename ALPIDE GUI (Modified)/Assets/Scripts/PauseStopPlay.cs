﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseStopPlay : MonoBehaviour {
	

	private FunnelIO DataFunnel;

	public GameObject Menu;

	public Sprite play;

	public Button button;

	private bool paused = false;

	private TrackMaker trackmaker;

	public Text tracktext;

	void Start()
	{
		trackmaker = Camera.main.GetComponent<TrackMaker> ();
		DataFunnel = Camera.main.GetComponent<FunnelIO> ();
	}
	
	public void Pause () {
		// pause/play data gathering

		if (Menu.activeSelf == false) {
			DataFunnel.startdata = !DataFunnel.startdata;
			if (paused == false) {
				button.image.overrideSprite = play;

			} else {
				button.image.overrideSprite = null;

			}

			paused = !paused;
		}




	}

	public void Stop ()
	{
		if (Menu.activeSelf == false) {
			trackmaker.numtracks = 0;
			tracktext.text = "Total Tracks: 0";
			DataFunnel.startdata = !DataFunnel.startdata;
			Menu.SetActive (true);
		}

	}
}
