﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.IO;
using System;
using System.Text;
using UnityEngine.UI;

public class FunnelIO : MonoBehaviour {

	// Data Movement
	public SerialPort stream; //input Sensor data stream
	private StreamWriter outStream; // writing data to file
	private string currentdata = null;//JSON data drawn from sensors
	public string[] datarray; // array housing data when split
	private string filePath;// for data saving
	private int timeoutcount = 0;// set amount before declaring connection has been lost
	public int timeoutval;//stream timeout value

	// Conditionals
	public bool startdata = false; //triggered from menu - starts/stops data gathering
	public bool[] datavalid = new bool[4]; // sent to tilemaps to trigger their calculations
	public bool[] shutoff = new bool[4]; // sent from tilemaps after completing a noise run
	public bool shutdown = false; // Close all streams and quit application
	public bool abortalldata = false; // discard data when invalid
	public bool portfound = false; // port has been detected
	public bool record = false; // decides whether or not tilemaps should save their data
	public bool saveoriginal = false; // save/don't save original data
	public bool planefour = false; // indicates whether 4 planes have been selected

	public bool planethree = true; // Added value for removal of 3 plane, used for two planes


	// Windows Port Selection
	public bool refreshports = true; // refresh the ports list
	public bool testconnection = false; // attempt connection
	public Dropdown portchoice; // port selection

	// Planes
	public GameObject[] planes; // the four different planes
	private TileMap p4; // plane 4
	private TileMap p3;
	private TileMap[] p2 = new TileMap[2]; // minimum two main planes

	// UI Elements
	public GameObject PortsUI; // contains Ports dropdown
	public GameObject Menu; // Main Menu
	public GameObject logtext;

	//Others
	private RuntimePlatform platform = Application.platform;//detects which platform is in use
	private TrackMaker TrackTest; //link to trackmaker script



	void Start () {
		// Initialisation
		planefour = false; // default no plane 4
		timeoutval = 10;//default 10 ms
		saveoriginal = false; // default dont save data
		testconnection = false; //default dont test connection
		refreshports = true;// default refresh ports list
		record = false;// default dont record
		portfound = false;// default port not found
		abortalldata = false;// default dont abort
		shutdown = false;//default do not quit

		// link to trackmaker
		TrackTest = Camera.main.GetComponent<TrackMaker> ();

		//assigns links to plane
		foreach (GameObject testplane in planes) {
			if (testplane.GetComponent<TileMap> ().planeno == 3) {
				p4 = testplane.GetComponent<TileMap> ();
			} else if (testplane.GetComponent<TileMap> ().planeno == 2) {
				p3 = testplane.GetComponent<TileMap>();
			} else {
				p2 [testplane.GetComponent<TileMap> ().planeno] = testplane.GetComponent<TileMap> ();
			}
		}

		//For windows, UI needs to be activated
		if (platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer) {
			PortsUI.SetActive (true);
		} else {
			PortsUI.SetActive (false);
		}

		//Main Function
		InvokeRepeating ("PullData",0.3f,0.3f); //repeating every 0.3 seconds

		//get file path for data saving and create an empty file(or wipe an existing one)
		filePath = getPath();
		outStream = System.IO.File.CreateText(filePath);
	}



	void PullData () {

		//Noise Run End
		if (shutoff [0] == true && shutoff [1] == true ) {// once noise run is ready, turn on menu and stop data flow
			
			if (shutoff [2] == true || planethree == false) { //Implemented Same Condition for 3rd plane
				if(planethree == true){
					shutoff [2] = false;
				}

				if (shutoff [3] == true || planefour == false) { // condition tested for 4th plane when this is active
					
					startdata = false;// turn off data flow
					shutoff [0] = shutoff [1] = false;// set values back to false
					if (planefour == true) {
						shutoff [3] = false;
					}

					Menu.SetActive (true);// turn on menu
				}
			}
		}

		//Port Detection and Data Gathering
		if (portfound == false) {
			StartCoroutine (getPortNames ());// continuously search for port if not found
			if (shutdown == true) { // quitting function
				SaveandQuit ();
			}
		} else {
			if (startdata == true) {
				if (TrackTest.maindone == true) {// pull data when calculations are ready
					if (shutdown == false) {
						ReadFromArduino (0);//main reading function
					} else {//only quit when all scripts have finished their data saving procedures
						SaveandQuit ();
					}
				}
			} else {
				if (shutdown == true) {//if data isn't flowing, quitting can happen at once
					SaveandQuit ();
				}
			}
		}

	}
	private void SaveandQuit()
	{//closes streams and exits
		if (portfound == true) {
			stream.Close (); // close stream if connected
		}
		outStream.Close();// always close data streams
		TrackTest.outStream.Close ();
		foreach(TileMap Plane in p2)
		{
			Plane.clusterStream.Close ();
			Plane.dataStream.Close ();
		}
		if (planethree == true) {
			p3.clusterStream.Close ();
			p3.dataStream.Close ();
		}

		if (planefour == true) {
			p4.clusterStream.Close ();
			p4.dataStream.Close ();
		}
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit ();
		#endif

	}
	private void ReadFromArduino (int timeout = 0) {
		abortalldata = false; // reset abort conditional
		try {
			currentdata = stream.ReadLine(); //attempts to read a full line and split it into its componenets
            //Debug.Log("Data recieved: ");
            //Debug.Log(currentdata.ToString());
            datarray = currentdata.Split ('|');
		}
		catch  {// if this timesout, indicate an error and skip run
			//Debug.LogError ("Timeout");
			timeoutcount++;
			currentdata = null;
		}
		if (currentdata != null) {
			//only accept data if it contains all three/four plane data
			//Changed to now accepting Data if it now contains 2,3 or 4 planes
			if ((datarray.Length == 2) || (datarray.Length == 3 && planethree == true) ||((datarray.Length == 4) && (planefour == true)) )  {
				foreach (TileMap Plane in p2) { // attempt to parse data; if this is irregular, discard and abort run
					try {
						Plane.Data = JsonUtility.FromJson<PixelHits>(datarray [Plane.GetComponent<TileMap>().planeno]);//collect hit data
						Plane.Data.nhits = Plane.GetComponent<TileMap> ().Data.nhits * 2;
					} catch (Exception e){
						//Debug.Log (e);
						//Debug.Log ("Data JSON error: " + datarray [Plane.GetComponent<TileMap>().planeno]);
						abortalldata = true;// abort run
					}

				}
				//Equal Implementation as for the fourth plane
				if (planethree == true) {
					try{
						p3.Data = JsonUtility.FromJson<PixelHits>(datarray[2]);
						p3.Data.nhits = p3.Data.nhits * 2;
					} catch {
						abortalldata = true;
					}

				}


				if (planefour == true) {//same thing for plane 4 if selected
					try {
						p4.Data = JsonUtility.FromJson<PixelHits>(datarray [3]);//collect hit data
						p4.Data.nhits = p4.Data.nhits * 2;
					
					} catch {
					//	Debug.Log (e);
					//	Debug.Log ("Data JSON error: " + datarray [3]);
						abortalldata = true;
					}
				}
					
				if (abortalldata == false) {// if successful, trigger calculations in other scripts
		
					if (record == true) { // if recording is indicated, pass on to tilemaps
						foreach (TileMap Plane in p2) {
							Plane.record = true;
						}
						if (planethree == true) {
							p3.record = true;
						}
						if (planefour == true) {
							p4.record = true;
						}

					} else {
						foreach (TileMap Plane in p2) {
							Plane.record = false;
						}
						if (planethree == true) {
							p3.record = false;
						}
						if (planefour == true) {
							p4.record = false;
						}
					}
					if (saveoriginal == true) { // save original data if indicated
						outStream.WriteLine (currentdata);
					}
					timeoutcount = 0;// reset timeoutcount since data has been successfully read
					TrackTest.maindone = false; // prevent dataflow until calculations have ended
					datavalid [0] = datavalid [1] = true; //trigger the plane scripts
					if (planethree == true){
						datavalid [2] = true;
					}
					if (planefour == true) {
						datavalid [3] = true;
					}
				} 
				else {
					abortalldata = false; // if data reading has failed, try another run.
				}

			} else {

				if (currentdata == "") { // if no data is read, treat as an error
					timeoutcount++;

				}
				currentdata = null;

			}
		}

		if (timeoutcount == 20) {// if data is skipped 20 times in a row, sever connection and attempt to find a new one
			Debug.Log ("Connection Lost.");
			portfound = false;
		}



	}

	private IEnumerator getPortNames ()
	{
		string[] ports = SerialPort.GetPortNames ();// works only on Windows

		if (platform == RuntimePlatform.OSXEditor || platform == RuntimePlatform.OSXPlayer) {
			// on a Mac, a port can be automatically be found since it is always assigned usbmodem or usbserial 
			string[] ttys = Directory.GetFiles ("/dev/", "tty.*");
		
			foreach (string dev in ttys) {

				if (dev.Contains ("usbmodem") || dev.Contains ("usbserial")) {
					AccessPort (dev);
				}
			}
				
		} else if (platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer) {
			// on Windows, the user needs to manually select the port from a drop-down menu

			if (refreshports == true) {//if requested by user, refresh ports list
				refreshports = false;// only once
				ports = SerialPort.GetPortNames ();
				portchoice.options.Clear ();
				foreach (string port in ports) {
					portchoice.options.Add (new Dropdown.OptionData () { text = port }); //add options to drop down menu
				}
				portchoice.value = 1;// refresh default value
				portchoice.value = 0;
			}

			if (testconnection == true) { // if indicated, attempt to connect to selected port
				AccessPort (ports [portchoice.value]);
			}

		} else if (platform == RuntimePlatform.LinuxEditor || platform == RuntimePlatform.LinuxPlayer) {
			// Linux works similary to Mac, but with different port names
			string[] ttys = Directory.GetFiles ("/dev/");
			foreach (string dev in ttys) {

				if (dev.Contains ("USB") || dev.Contains ("ACM")) {
					AccessPort ("/dev/ttyUSB0");
				}
			}
		}

		yield return null;
	}

	private void AccessPort(string portname)
	{
		stream = new SerialPort (portname, 115200);// baud rate set to 115200
		stream.ReadTimeout = timeoutval; // set by user (default 10 ms)
		try {
			stream.Open ();
			portfound = true; // indicate connection has been established
			testconnection = false;
			StartCoroutine(logmessage(portname));
		//	Debug.Log ("Correct Port: " + portname);
		} catch {
		//	Debug.Log ("Couldn't establish connection, retrying...");

		}

	}
	private IEnumerator logmessage (string message)
	{
		int nextupdate = Mathf.FloorToInt (Time.time) + 5;
		logtext.SetActive(true);
		while (Time.time < nextupdate) {

			logtext.GetComponent<Text>().text = "Port Found: " + message;

			yield return  new WaitForSeconds (0.02f);
		}
		logtext.SetActive(false);

		yield return null;

	}

	private string getPath(){
				#if UNITY_EDITOR
				return Application.dataPath + "/CSV/Data/" + "Original_Stream.csv";
				#else
				return Application.dataPath +"/"+"Original_Stream.csv";
				#endif
	}

}
