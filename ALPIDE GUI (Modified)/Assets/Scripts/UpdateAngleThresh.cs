﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateAngleThresh : MonoBehaviour {

	private TrackMaker trackmaker;
	public InputField inputangle;
	private int maxangle;
	// Use this for initialization
	void Start () {
		trackmaker = Camera.main.GetComponent<TrackMaker> ();

	}

	// Update is called once per frame
	public void ChangeAngleThresh () {
		if (trackmaker.planefour == true) {
			maxangle = 360;
		} else {
			maxangle = 180;
		}
			
		if (int.Parse (inputangle.text) <= maxangle) {
			trackmaker.anglethresh = int.Parse (inputangle.text);
		} else {
			trackmaker.anglethresh = maxangle;
			inputangle.text = maxangle.ToString();
		}
	}
}
