﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectLineAnchor : MonoBehaviour {


	public GameObject Cylinder;
	private GameObject objectx;
	private GameObject objecty;
	public Transform transform1;
	public Transform transform2;
	public bool trigger = false;
	private float distance;
	private float distance2;
//	private Plane xy;
	public GameObject anchor;
	private Vector3 z;


	private string currentdata = "/dev/|tty.usbmodem1411";//JSON data drawn from port
	public string[] datarray;



	// Use this for initialization
	void Start () {

		//    ConnectCylinder (transform1,transform2);
		//anchor = new GameObject("Anchor");

		distance = Vector3.Distance (transform1.position, transform2.position);
		objectx = Instantiate (Cylinder, transform1.position, Quaternion.identity);//transform1.rotation);//(transform1.rotation-transform2.rotation)/2);

		objectx.transform.LookAt(transform2);
		objectx.transform.Rotate (90, 0, 0);
		anchor.transform.position = transform1.position;
		anchor.transform.rotation = objectx.transform.rotation;
	//	xy = new Plane (Vector3.up, new Vector3 (0,0,0));
		anchor.transform.position += anchor.transform.up *3.236459f * 17.56f ;

		objecty = Instantiate (Cylinder, objectx.transform.position- (objectx.transform.up * 500f), Quaternion.identity);//transform1.rotation);//(transform1.rotation-transform2.rotation)/2);
		//objectx.transform.position += objectx.transform.up * 50f;
		objecty.transform.LookAt(transform2);
		objecty.transform.Rotate (90, 0, 0);
		distance2 =  Vector3.Distance (transform1.position, objecty.transform.position);



		datarray = currentdata.Split ('|');
		if (datarray.Length <= 2) {
			foreach (string datax in datarray) {
				Debug.Log (datax);
			}
		}
		currentdata = "/dev/|tty.us|bmodem1411";
		datarray = currentdata.Split ('|');
		if (datarray.Length <= 2) {
			foreach (string datax in datarray) {
				Debug.Log (datax);
			}
		}


	}

	// Update is called once per frame
	void Update () {
		objecty.transform.localScale = new Vector3(objecty.transform.localScale.x,Mathf.Lerp (objecty.transform.localScale.y, distance2/17.56f, 0.1f * Time.deltaTime),objecty.transform.localScale.z);    

		if (objecty.transform.localScale.y >= ((distance2 / 17.56f) * 0.8)) {
	     	objectx.transform.localScale = new Vector3(objectx.transform.localScale.x,Mathf.Lerp (objectx.transform.localScale.y, distance/17.56f, 0.01f * Time.deltaTime),objectx.transform.localScale.z);    

		}
		//Debug.Log(xy.GetDistanceToPoint(anchor.transform.position));
	//	anchor.transform.position += anchor.transform.up * objectx.transform.localScale.y/17.56f; //= new Vector3 (anchor.transform.localPosition.x,objectx.transform.localScale.y/17.56f,anchor.transform.localPosition.z);
		//anchor.transform.position += anchor.transform.up * Time.deltaTime * 1f;

		anchor.transform.position = Vector3.Lerp (anchor.transform.position, transform2.position, 0.005f * Time.deltaTime);	
		//Debug.Log (anchor.transform.position);
	}



	void ConnectCylinder(Transform start, Transform end)
	{
		float distance = Vector3.Distance (start.position, end.position);
		Debug.Log (distance);
		//    Vector3 z = (start.position - end.position)/2;
		//    if
		objectx = Instantiate (Cylinder, start.position, Quaternion.identity);//transform1.rotation);//(transform1.rotation-transform2.rotation)/2);
		objectx.transform.LookAt(end);
		objectx.transform.Rotate (90, 0, 0);
		//    Vector3 x = start.position - end.position;
		//    Debug.Log (x);
		objectx.transform.localScale = new Vector3(objectx.transform.localScale.x,distance/17.56f,objectx.transform.localScale.z); //17.65 due to maya import settings etc
		//    3.236459
	}
}