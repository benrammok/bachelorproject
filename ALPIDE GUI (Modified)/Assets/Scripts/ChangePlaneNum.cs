﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ChangePlaneNum : MonoBehaviour {

	// Use this for initialization
	private FunnelIO DataFunnel;
	private TrackMaker trackmaker;
	public GameObject Height23;
	public Dropdown planenums;
	public GameObject p4;
	public GameObject Hits;
	public GameObject Noise;

	//Gameobjects for third plane
	public GameObject Height12;
	public GameObject p3;
	public GameObject Hits3;
	public GameObject Noise3;

	void Start () {
		
		DataFunnel = Camera.main.GetComponent<FunnelIO> ();
		trackmaker = Camera.main.GetComponent<TrackMaker> ();
	}
	
	// Update is called once per frame
	public void DealWithChange () {

		if (planenums.value == 0) {
			
			//Deactivate Plane 3
			DataFunnel.planethree = false;
			trackmaker.planethree = false;
			Height12.SetActive(false);
			p3.SetActive(false);
			Hits3.SetActive (false);
			Noise3.SetActive (false);

			//Deactivate Plane 4
			DataFunnel.planefour = false;
			Height23.SetActive(false);
			p4.SetActive (false);
			Hits.SetActive (false);
			Noise.SetActive (false);
		}
		else if (planenums.value == 1) {
			//Activate Plane 3
			DataFunnel.planethree = true;
			trackmaker.planethree = true;
			Height12.SetActive(true);
			p3.SetActive(true);
			Hits3.SetActive (true);
			Noise3.SetActive (true);


			DataFunnel.planefour = false;
			trackmaker.planefour = false;
			Height23.SetActive (false);
			p4.SetActive (false);
			Hits.SetActive (false);
			Noise.SetActive (false);

		} else {
			DataFunnel.planethree = true;
			trackmaker.planethree = true;
			Height12.SetActive(true);
			p3.SetActive(true);
			Hits3.SetActive (true);
			Noise3.SetActive (true);

			DataFunnel.planefour = true;
			trackmaker.planefour = true;
			Height23.SetActive (true);
			p4.SetActive (true);
			Hits.SetActive (true);
			Noise.SetActive (true);

		}
		
	}
}
