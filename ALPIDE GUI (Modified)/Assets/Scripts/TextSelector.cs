﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextSelector : MonoBehaviour {
	
	public Transform newtarget;
	private Color originalColor;
	private GameObject[] planes;
	void Start ()
	{
		originalColor = GetComponent <TextMesh> ().color;
	}

	void OnMouseEnter () //changes the colour of text
	{
		GetComponent <TextMesh> ().color = Color.red;
	}

	void OnMouseExit ()
	{
		GetComponent<TextMesh> (). color = originalColor;
	}

	void OnMouseUpAsButton () //moves the camera to view the instructions
	{
		Camera.main.GetComponent<CameraTargetFollower>().target = newtarget;
		Camera.main.GetComponent<CameraTargetFollower>().enable = true;
		Camera.main.orthographic = false;
		Camera.main.GetComponent<FunnelIO> ().startdata = true;
		planes = GameObject.FindGameObjectsWithTag ("Plane");

		foreach (GameObject Plane in planes) {

			Plane.GetComponent<TileMap> ().applydata = true;
		}

	}
}
