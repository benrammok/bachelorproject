﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using UnityEngine;
using System.IO.Ports;
using System.IO;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class TestIONEW : MonoBehaviour {


	private SerialPort stream;//= new SerialPort("/dev/tty.usbmodem1411", 115200);
	private string currentdata = null;//JSON data drawn from port
	public bool shutdown = false;
	private int num = 0;

	private string filePath;
	//private string x = null;
	private int nextupdate = 1 ;
	private int numnew = 0;
	void Start () {
		StartCoroutine ("getPortNames");
		getPortNames ();
		stream.Open();
		stream.ReadTimeout = 1;
	//	Debug.Log (stream.ReadTimeout);
	//	StartCoroutine(PullData());
		InvokeRepeating ("PullData",0.2f,0.2f); //repeating every 0.5 seconds
		shutdown = false;

	}

	void Update(){

		if (Time.time >= nextupdate) {
			
			Debug.Log (numnew-num);
			num = numnew;
			nextupdate = Mathf.FloorToInt (Time.time) + 1; // 5 second delay

		}


	}
	private void PullData () {

	//	while (true) {
			if (shutdown == true) {
				stream.Close ();
				#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
				#else
			Application.Quit ();
				#endif
			}

			ReadFromArduino ();
		//	yield return new WaitForSeconds (0.2f);
	//	}

	}

	private void ReadFromArduino () {
		//stream.ReadTimeout = 0;//timeout; 

		try {
		//	stream.
			//stream.ReceivedBytesThreshold
			//stream.data

	//		Debug.Log(x);

				currentdata = stream.ReadLine();
			//	Debug.Log(currentdata);
			numnew++;
			
		}
		catch  {
			//Debug.LogError (e);
	//		Debug.LogError ("Timeout");
			currentdata = null;
		}

	}

	private IEnumerator getPortNames ()
	{
		int p = (int)Environment.OSVersion.Platform;

		// Are we on Unix?
		if (p == 4 || p == 128 || p == 6) {
			string[] ttys = Directory.GetFiles ("/dev/", "tty.*");
			foreach (string dev in ttys) {
				if(dev.Contains("usbmodem")||dev.Contains("usbserial"))
				{
					Debug.Log ("Correct Port:" + dev);
					stream = new SerialPort (dev, 115200);
				}
			}
		}
		yield return null;
	}


}
