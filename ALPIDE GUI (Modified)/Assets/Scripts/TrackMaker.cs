﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO.Ports;
using System.IO;
using System;
using System.Text;

public class TrackMaker : MonoBehaviour {


	//numerical quantities
	private float dividerx =  2000f/1024f; //dividers used for matching the number of pixels to the gameobject size
	private float dividerz =  1000f/512f;
	private int nextupdate = 7; // delays a new track from forming for 5 seconds
	private float[] distance = new float[5];// used for calculating cylinder positions
	public int numtracks;
	private int[] numbatches = new int[4];
	private int z = 0;
	public float enlarger = 8f;// size of tracks
	public int lifetime = 7; // lifetime of tracks
	public float[] Heights = new float[3];// plane heights
	public int anglethresh = 178;// cut off angle threshold

	//conditionals
	public bool[] triggers = new bool[4]{false,false,false,false}; // triggers sent from the three/four planes indicating the amount of hits is within limits
	private bool triggermain = false;
	public bool maindone = true;
	public bool planefour = false;
	public bool planethree = false;

	//Objects for marking Tracks
	public GameObject Cylinder; //Cylinder prefab used for track
	public Vector3[] inittranspoint = new Vector3[4]; //central point for each plane
	private Vector3 point1;
	private Vector3 point2;
	private Vector3 point3;
    //Making it clearer when using 4 planes
    private Vector3 point4; 

	//Planes
	public GameObject[] planes; // the four different planes
	public Transform[] datapos = new Transform[4]; // transforms representing the positioning of the tracks
	private TileMap p4;
	private TileMap[] p2 = new TileMap[2];
	//TileMap for third plane
	private TileMap p3;


	//data arrays
	public float[,] av0 = new float[150,2];// data obtained from tilemaps
	public float[,] av1 = new float[150,2];
	public float[,] av2 = new float[150,2];
	public float[,] av3 = new float[150,2];
	public int[] clusters = new int[4]; // number of clusters in each
	private float[] bestpoints = new float [8]; // datapoints chosen to have the straightest track 

	// UI
	public Text tracks;
	public Text angle;

	// Data Saving
	private string filePath;
	public StreamWriter outStream;

	void Start () {
		//Defaults
		planefour = false; //default no 4th plane
		planethree = true;
		anglethresh = 2;
		lifetime = 7;
		enlarger = 8f;
		triggers [0] = triggers [1] = triggers [2] =  triggers[3] = false;
		triggermain = false;
		maindone = true;
		numtracks = 0;

		// partitioning planes
		foreach (GameObject testplane in planes) {
			if (testplane.GetComponent<TileMap> ().planeno == 3) {
				p4 = testplane.GetComponent<TileMap>();
			} else if(testplane.GetComponent<TileMap>().planeno == 2){
				p3 = testplane.GetComponent<TileMap> ();
			} else {
				p2 [testplane.GetComponent<TileMap> ().planeno] = testplane.GetComponent<TileMap> ();
			}
		}
		//Data config
		filePath = getPath();
		outStream = System.IO.File.CreateText(filePath);

		tracks.text = " ";
		angle.text = " ";

	}
	
	void FixedUpdate () {

		CheckTrigger (); // triggers if planes are ready
		if (triggermain == true) {
			TriggerHandler ();// handles trigger, works with angles etc
		}
	
	}

	void CheckTrigger()
	{


		triggermain = true;
		foreach (TileMap Plane in p2) {// only triggers if the three planes all signal the done condition
			if (Plane.done == false) {
				triggermain = false;
			}
		}
		if (planethree == true) {
			if (p3.done == false) {
				triggermain = false;
			}
		}
		if (planefour == true) {
			if (p4.done == false) {
				triggermain = false;
			}
		}

	}

	void TriggerHandler()
	{
		foreach (TileMap Plane in p2) { // make sure this only happens once
			Plane.done = false;
		}
		if (planethree == true) {
			p3.done = false;
		}
		if (planefour == true) {
			p4.done = false;
		}

		//add another trigger here
		if (triggers [0] == true && triggers [1] == true) {// if all three planes signal a track
			if(triggers[2] == true || planethree == false){
				if (triggers [3] == true || planefour == false) {
					triggers [0] = triggers [1] = triggers [2] = false;// only once
					if (planefour == true) {
						triggers [3] = false;
						numbatches [3] = p4.numbatches;
					}
					if (planethree == true) {
						triggers [2] = false;
						numbatches [2] = p3.numbatches;
					}

					z = 0;

					foreach (TileMap Plane in p2) { // collects batch number ( for data validation)
							
						numbatches [z] = Plane.numbatches;
						z++;
					}
					StartCoroutine (DetermineTrack (av0, av1, av2, av3, clusters)); // determine track positions
				}
			} 
		}
			maindone = true; // signals funnel to take in more data
	}

	private IEnumerator DetermineTrack(float[,] lav0, float[,]lav1,float[,]lav2,float[,] lav3,int[] lclusters)
	{
		float bestangle = 0.0f;
		float newangle;
		float newanglea;
		float newangleb;
		float newanglefour;
		float bestanglefour = 0.0f;
		float bestanglethree = 0.0f;
		for (int i = 0; i < lclusters[0]; i++) { // for each combination of hits, measure the deviation from a straight angle
			for (int j = 0; j < lclusters [1]; j++) {
				if (planethree == true) {

					for (int k = 0; k < lclusters [2]; k++) {

						if (planefour == true) {

							for (int l = 0; l < lclusters [3]; l++) {

								point1 = new Vector3 (lav0 [i, 0], 0f, lav0 [i, 1]);
								point2 = new Vector3 (lav1 [j, 0], Heights [0], lav1 [j, 1]);
								point3 = new Vector3 (lav2 [k, 0], Heights [1] + Heights [0], lav2 [k, 1]);

								newanglea = FindAngle (point1, point2, point3);

								point2 = new Vector3 (lav1 [j, 0], Heights [0], lav1 [j, 1]);
								point3 = new Vector3 (lav2 [k, 0], Heights [0] + Heights [1], lav2 [k, 1]);
								point4 = new Vector3 (lav3 [l, 0], Heights [0] + Heights [1] + Heights [2], lav3 [l, 1]);

								newangleb = FindAngle (point2, point3, point4);

								newanglefour = newanglea + newangleb;

								if (newanglefour > bestanglefour) {
									bestanglefour = newanglefour;
									bestpoints [0] = lav0 [i, 0];
									bestpoints [1] = lav0 [i, 1];
									bestpoints [2] = lav1 [j, 0];
									bestpoints [3] = lav1 [j, 1];
									bestpoints [4] = lav2 [k, 0];
									bestpoints [5] = lav2 [k, 1];
									bestpoints [6] = lav3 [l, 0];
									bestpoints [7] = lav3 [l, 1];
								}

							}
						} else {
						
							point1 = new Vector3 (lav0 [i, 0], 0f, lav0 [i, 1]);
							point2 = new Vector3 (lav1 [j, 0], Heights [0], lav1 [j, 1]);
							point3 = new Vector3 (lav2 [k, 0], Heights [1] + Heights [0], lav2 [k, 1]);
					
							newangle = FindAngle (point1, point2, point3);

							if (newangle > bestanglethree) {
								bestpoints [0] = lav0 [i, 0];
								bestpoints [1] = lav0 [i, 1];
								bestpoints [2] = lav1 [j, 0];
								bestpoints [3] = lav1 [j, 1];
								bestpoints [4] = lav2 [k, 0];
								bestpoints [5] = lav2 [k, 1];
								bestanglethree = newangle;

							}
						}
					}
				} else {
                    //Checks the angle between the resultant vectors
					point1 = new Vector3 (lav0 [i, 0], 0f, lav0 [i, 1]);
					point2 = new Vector3 (lav1 [j, 0], Heights [0], lav1 [j, 1]);

                    //When using two planes we could chose a cut-off
				    newangle = FindAngle(point1,point2);

					if (newangle > bestangle) {
						bestpoints [0] = lav0 [i, 0];
						bestpoints [1] = lav0 [i, 1];
						bestpoints [2] = lav1 [j, 0];
						bestpoints [3] = lav1 [j, 1];
						bestangle = newangle;

					}
				}
			}
		}

		if (planethree == false) {

			angle.text = "Best Angle: " + bestangle.ToString ("#.00");

			outStream.WriteLine ("Angle " + bestangle.ToString () + " Event " + numbatches [0].ToString ());
			if (bestangle > (180 - anglethresh)) {
				numtracks += 1;
				tracks.text = "Total Tracks: " + numtracks.ToString ();
				Cylindermaker (bestpoints);  // stretch cylinder between positions
			}

		} else {
			if (planefour == false) {
			
				angle.text = "Best Angle: " + bestanglethree.ToString ("#.00");

				outStream.WriteLine ("Angle " + bestanglethree.ToString () + " Event " + numbatches [0].ToString ());

				if (bestanglethree > (180 - anglethresh)) {
					numtracks += 1;
					tracks.text = "Total Tracks: " + numtracks.ToString ();
					Cylindermaker (bestpoints);  // stretch cylinder between positions
				}
			} else {
				angle.text = "Best Angle: " + bestanglefour.ToString ("#.00");

				outStream.WriteLine ("Angle " + bestangle.ToString () + ",Event " + numbatches [0].ToString ());

				if (bestanglefour > (360 - anglethresh)) {
					numtracks += 1;
					tracks.text = "Total Tracks: " + numtracks.ToString ();
					Cylindermaker (bestpoints);  // stretch cylinder between positions
				}

			}
		}
		yield return null;
	}

	void Cylindermaker(float[] startpoints)
	{
		 GameObject trackstage3;
		 GameObject trackstage2; //instantiating tracks
		 GameObject trackstage1;
		 GameObject trackorigin;
		 GameObject tracktermination;
		// translates and selects datapositions based on the data gathered from the angle measurement
		Transform hit0 = datapos [0];
		Transform hit1 = datapos [1];
		Transform hit2 = datapos [2];
		Transform hit3 = datapos [3];
        Transform temphit;

		hit0.position = 	inittranspoint[0] - new Vector3 (1000, 0, 500) + new Vector3 (startpoints[0] * dividerx, 0.0f, startpoints[1] * dividerz) + new Vector3 (0.5f * dividerx, 0, 0.5f * dividerz);
		hit1.position =  	inittranspoint[1] - new Vector3 (1000, 0, 500) + new Vector3 (startpoints[2] * dividerx, 0.0f, startpoints[3] * dividerz) + new Vector3 (0.5f * dividerx, 0, 0.5f * dividerz);
        temphit = hit0;
		if (planefour == true) {
			hit3.position = inittranspoint [3] - new Vector3 (1000, 0, 500) + new Vector3 (startpoints [6] * dividerx, 0.0f, startpoints [7] * dividerz) + new Vector3 (0.5f * dividerx, 0, 0.5f * dividerz);
			trackstage3 = ConnectCylinder (hit3, hit2, 4);

		} else {
			trackstage3 = ConnectCylinder (hit1, hit0, 4); // dummy value
		}

        //Conditional for if three boards are present
		if (planethree == true){
			hit2.position = 	inittranspoint[2] - new Vector3 (1000, 0, 500) + new Vector3 (startpoints[4] * dividerx, 0.0f, startpoints[5] * dividerz) + new Vector3 (0.5f * dividerx, 0, 0.5f * dividerz);
			trackstage1  = ConnectCylinder (hit2,hit1, 1);
		}else{
			trackstage1  = ConnectCylinder (hit1,hit0, 1);
		}

		trackstage2  = ConnectCylinder (hit1,hit0,0);

        if (planefour == false)
        {
            if (planethree == false) {
                hit0.position = hit1.position - (trackstage1.transform.up * 2000f);
                trackorigin = ConnectCylinder(hit0, hit1, 2);
            }
            else
            {
                hit1.position = hit2.position - (trackstage1.transform.up * 2000f);//default 500.....
                trackorigin = ConnectCylinder(hit1, hit2, 2);
            }
        }
        else
        {
			hit1.position = hit3.position - (trackstage1.transform.up * 2000f);//default 500.....
			trackorigin  = ConnectCylinder (hit1,hit3,2);
		}
        

		hit1.position = temphit.position + (trackstage2.transform.up * 2000f);
		tracktermination  = ConnectCylinder (temphit,hit1,3);

		trackstage2.SetActive (false); // only the tip should show at first
		trackstage1.SetActive (false);
		trackstage3.SetActive (false);
		trackorigin.SetActive (true);
		tracktermination.SetActive (false);

		nextupdate = Mathf.FloorToInt (Time.time) + lifetime; // 5 second delay
		StartCoroutine (LerpTracks (trackstage1,trackstage2,trackstage3,trackorigin,tracktermination,nextupdate));

	}

	GameObject ConnectCylinder(Transform start, Transform end, int ind)
	{
		GameObject Object;
		distance[ind] = Vector3.Distance (start.position, end.position);
		Object = Instantiate (Cylinder, start.position, Quaternion.identity);//transform1.rotation);//(transform1.rotation-transform2.rotation)/2);
		Object.transform.LookAt(end);
		Object.transform.Rotate (90, 0, 0);
		return Object;
	}



	float FindAngle (Vector3 p1, Vector3 p2, Vector3 p3 )
	{// finds the best angle between the two vectors formed from 3 planes

		float x_pixel = 0.2924f;// pixel dimensions
		float z_pixel = 0.2688f;

		p1 =  new Vector3 (p1.x*x_pixel,p1.y, p1.z*z_pixel);
		p2 =  new Vector3 (p2.x*x_pixel,p2.y, p2.z*z_pixel);
		p3 =  new Vector3 (p3.x*x_pixel,p3.y, p3.z*z_pixel);

		Vector3 p21 = p1 - p2;
		Vector3 p23 = p3 - p2;

		return Vector3.Angle (p21, p23);

	}

	float FindAngle (Vector3 p1, Vector3 p2)
	{// finds the best angle between vectors formed from 2 planes

		float x_pixel = 0.2924f;// pixel dimensions
		float z_pixel = 0.2688f;

		p1 =  new Vector3 (p1.x*x_pixel,p1.y, p1.z*z_pixel);
		p2 =  new Vector3 (p2.x*x_pixel,p2.y, p2.z*z_pixel);
        Vector3 horisVect = new Vector3(x_pixel, 0, z_pixel);
        Vector3 p21 = p1 - p2;
		return Vector3.Angle (p21, horisVect);

	}

	private string getPath(){


		#if UNITY_EDITOR
		return Application.dataPath + "/CSV/Data/" + "Angles.csv";
		#else
		return Application.dataPath +"/"+"Angles.csv";
		#endif

	}


	private IEnumerator LerpTracks(GameObject ts1, GameObject ts2, GameObject ts3, GameObject to, GameObject tt,int persistence)
	{

		while (Time.time < persistence) {

			to.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (to.transform.localScale.y, distance [2] / 17.56f, 5f * Time.deltaTime), enlarger);

			if (planefour == true) {
				
				if (to.transform.localScale.y >= ((distance[2] / 17.56f) * 0.9)) {
					ts3.SetActive (true);
					ts3.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (ts3.transform.localScale.y, distance [4] / 17.56f, 5f * Time.deltaTime), enlarger);	
				}
				if (ts3.transform.localScale.y >= ((distance[4] / 17.56f) * 0.9)) {
					ts1.SetActive (true);
					ts1.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (ts1.transform.localScale.y, distance [1] / 17.56f, 5f * Time.deltaTime), enlarger);
				}
				if (ts1.transform.localScale.y >= ((distance[1] / 17.56f) * 0.9)) {
					ts2.SetActive (true);
					ts2.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (ts2.transform.localScale.y, distance [0] / 17.56f, 5f * Time.deltaTime), enlarger);
				}
				if (ts2.transform.localScale.y >= ((distance[0] / 17.56f) * 0.9)) {
					tt.SetActive (true);
					tt.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (tt.transform.localScale.y, distance [3] / 17.56f, 5f * Time.deltaTime), enlarger);
				}

			} else if (planethree == true) {
				if (to.transform.localScale.y >= ((distance[2] / 17.56f) * 0.9)) {
					ts1.SetActive (true);
					ts1.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (ts1.transform.localScale.y, distance [1] / 17.56f, 5f * Time.deltaTime), enlarger);	
				}
				if (ts1.transform.localScale.y >= ((distance[1] / 17.56f) * 0.9)) {
					ts2.SetActive (true);
					ts2.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (ts2.transform.localScale.y, distance [0] / 17.56f, 5f * Time.deltaTime), enlarger);
				}
				if (ts2.transform.localScale.y >= ((distance[0] / 17.56f) * 0.9)) {
					tt.SetActive (true);
					tt.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (tt.transform.localScale.y, distance [3] / 17.56f, 5f * Time.deltaTime), enlarger);
				}
			} else {
                //NEEDS to be improved, however this does work!
				if (to.transform.localScale.y >= ((distance[2] / 17.56f) * 0.9)) {
					ts1.SetActive (true);
					ts1.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (ts1.transform.localScale.y, distance [1] / 17.56f, 5f * Time.deltaTime), enlarger);
                    tt.SetActive(true);
                    tt.transform.localScale = new Vector3(enlarger, Mathf.Lerp(tt.transform.localScale.y, distance[3] / 17.56f, 5f * Time.deltaTime), enlarger);
				}
                /*
				if (ts1.transform.localScale.y >= ((distance[1] / 17.56f) * 0.9)) {
                    Debug.Log("Setting Track between planes 0 and 1");
					ts2.SetActive (true);
					ts2.transform.localScale = new Vector3 (enlarger, Mathf.Lerp (ts2.transform.localScale.y, distance [0] / 17.56f, 5f * Time.deltaTime), enlarger);
                  
				}
                */
			}
	
			yield return new WaitForSeconds (0.02f);
		}
		if (Time.time >= persistence) { //delays track for 5 seconds
			Destroy (ts1);
			Destroy (ts2);
			Destroy (ts3);
			Destroy (tt);
			Destroy (to);

		}
		yield return null;
	}

}
