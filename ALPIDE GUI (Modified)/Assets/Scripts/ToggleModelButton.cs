﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using UnityEngine;

public class ToggleModelButton : MonoBehaviour {


	public GameObject[] models; // the three different planes
	private bool change = false;
	private float enlarger = 1f;
	private TrackMaker trackmaker;
	void Start()
	{
		change = false;
		trackmaker = Camera.main.GetComponent<TrackMaker> ();

	//	models = GameObject.FindGameObjectsWithTag ("Models");
	}

	public void Toggle()
	{

		foreach (GameObject Model in models) {
			Model.SetActive (change);

		}

		if (change == true) {
			enlarger = 8f;
		} else {
			enlarger = 1f;
		}
		trackmaker.enlarger = enlarger;
		change = !change;

	}

}
