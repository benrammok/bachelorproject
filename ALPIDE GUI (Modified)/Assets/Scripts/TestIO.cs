﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System;

public class TestIO : MonoBehaviour {

	private SerialPort stream = new SerialPort("/dev/cu.usbmodem1421", 115200);

	// Use this for initialization
	void Start () {
		stream.ReadTimeout = 50;
		stream.Open();
	
	}
	
	// Update is called once per frame
	void Update () {
		//stream.WriteLine ("p");
	   // stream.BaseStream.Flush();
		StartCoroutine
		(
			AsynchronousReadFromArduino
			(   (string s) => Debug.Log(s),     // Callback
				() => Debug.LogError("Error!"), // Error callback
				10f                             // Timeout (seconds)
			)
		);
	}


	//public string ReadFromArduino (int timeout = 0) {      
		//	return stream.ReadLine();

	//}
	public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity) {
		DateTime initialTime = DateTime.Now;
		DateTime nowTime;
		TimeSpan diff = default(TimeSpan);

		string dataString = null;

		do {
			try {
				dataString = stream.ReadLine();
			}
			catch (TimeoutException) {
				dataString = null;
			}

			if (dataString != null)
			{
				callback(dataString);
				yield return null;
			} else
				yield return new WaitForSeconds(0.05f);

			nowTime = DateTime.Now;
			diff = nowTime - initialTime;

		} while (diff.Milliseconds < timeout);

		if (fail != null)
			fail();
		yield return null;
	}
}
