﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectLine : MonoBehaviour {


	public GameObject Cylinder;
	private GameObject objectx;
	public Transform transform1;
	public Transform transform2;
	public bool trigger = false;
	// Use this for initialization
	void Start () {

		ConnectCylinder (transform1,transform2);
	}
	
	// Update is called once per frame
	void Update () {


	}

//
//	void ConnectCylinder()
//	{
//
//		Vector3 z = (transform1.position - transform2.position)/2;
//		//	if
//		objectx = Instantiate (Cylinder, transform1.position - z, Quaternion.identity);//transform1.rotation);//(transform1.rotation-transform2.rotation)/2);
//		objectx.transform.LookAt(transform2);
//		objectx.transform.Rotate (90, 0, 0);
//	//	Vector3 x = transform1.position - transform2.position;
//		float distance = Vector3.Distance (transform1.position, transform2.position);
//	//	Debug.Log (x);
//
//	//	Debug.Log (x);
//		objectx.transform.localScale = new Vector3(objectx.transform.localScale.x,distance/2,objectx.transform.localScale.z);
//
//	}


	void ConnectCylinder(Transform start, Transform end)
	{
		float distance = Vector3.Distance (start.position, end.position);
		Vector3 z = (start.position - end.position)/2;
		//	if
		objectx = Instantiate (Cylinder, start.position - z, Quaternion.identity);//transform1.rotation);//(transform1.rotation-transform2.rotation)/2);
		objectx.transform.LookAt(end);
		objectx.transform.Rotate (90, 0, 0);
		//	Vector3 x = start.position - end.position;
		//	Debug.Log (x);
		objectx.transform.localScale = new Vector3(objectx.transform.localScale.x,distance/2,objectx.transform.localScale.z);

	}
}
