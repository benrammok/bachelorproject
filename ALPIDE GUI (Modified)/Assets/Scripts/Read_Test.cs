﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;
using System.IO.Ports;
public class Read_Test : MonoBehaviour {

	private IEnumerator datasaver;
	private int num_mask = 0;
	private int[,] mask = new int[80,2];//masking array
	// Use this for initialization
	void Start () {
		datasaver = SaveData ();// setting up IENumerator
		StartCoroutine (datasaver);

	}
	private IEnumerator SaveData()
	{

		// Creating First row of titles manually..
//		string[] rowDataTemp = new string[2];
//		rowDataTemp[0] = "Mask Plane " + planeno;
//		rowDataTemp[1] = "" + num_mask;
//		rowData.Add(rowDataTemp);
//
//		// You can add up the values in as many cells as you want.
//		for(int i = 0; i < num_mask; i++){
//			rowDataTemp = new string[2];
//			rowDataTemp[0] = "" + mask[i,0]; // name
//			rowDataTemp[1] = ""+mask[i,1]; // ID
//			rowData.Add(rowDataTemp);
//		}
//
//		string[][] output = new string[rowData.Count][];
//
//		for(int i = 0; i < output.Length; i++){
//			output[i] = rowData[i];
//		}
//
//		int     length         = output.GetLength(0);
//		string     delimiter     = ",";
//
//		StringBuilder sb = new StringBuilder();
//
//		for (int index = 0; index < length; index++)
//			sb.AppendLine(string.Join(delimiter, output[index]));
		
		string filePath = getPath();


			StreamReader inStream = new StreamReader (filePath);

		string[] s = inStream.ReadLine ().Split (',');
		num_mask = Convert.ToInt32 (s [1]);
		for (int i =0; i < num_mask ;i++)
		{
			s = inStream.ReadLine ().Split (',');
			mask [i, 0] = Convert.ToInt32 (s [0]);
			mask [i, 1] = Convert.ToInt32 (s [1]);
		}

			inStream.Close ();

		yield return null;
	}
	private string getPath(){
		#if UNITY_EDITOR
		return Application.dataPath +"/CSV/"+"Saved_data_plane_0.csv";
		#else
		return Application.dataPath +"/"+"Saved_data_plane_0.csv";
		#endif
	}
}
