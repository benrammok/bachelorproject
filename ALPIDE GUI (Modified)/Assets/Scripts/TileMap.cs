﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;
using System.IO.Ports;
using UnityEngine.UI;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(MeshRenderer))]
//[ExecuteInEditMode]

public class PixelHits // takes care of JSON input
{
	public int nhits;
	public int[] hits;
}

public class TileMap : MonoBehaviour {


	// numerical quantities
	public int planeno = 0; // indicates which plane on the stack is this
	private float dividerx =  2000f/1024f;//scaling factors
	private float dividerz =  1000f/512f;
	private int position = 0;// used for clusters
	private int num_mask = 0;//number of masked pixels
	private int masked = 0;
	private int testnum = 0; // current mask check
	public int maskchecks = 10; //number of tries to check for masking
	public int maskthreshold = 2; //number of hits to be included in mask
	private int bluepix = 0;
	private int nextentry = 1;
	private int numnonmasked = 0;
	private int clustertotal = 0;
	private int type = 0; //noise/data/clusters
	public int numbatches = 0;

	// Data storage
	private string FilePathNoise;
	private string FilePathData;
	private string FilePathClusters;
	private StreamWriter noiseStream;
	public StreamWriter dataStream;
	public StreamWriter clusterStream;
	private List<string[]> rowData = new List<string[]>();// used when saving data

	// conditionals
	public bool applydata = false;
	private bool maskready = false;
	public bool done = false;
	public bool record = false;
	public bool measuring = false; // switches between measuring hits or masking noise


	//Data arrays
	public PixelHits Data = new PixelHits();//data obtained from Arduino stream
	private PixelHits DemaskedData = new PixelHits();//data obtained from Arduino stream, demasked
	private int[,] repeatedreadings = new int[100000,3];//array containing repeated pixel hits - noisy pixels
	private int[,] mask = new int[500,2];//masking array
	private int [,] clustersfound = new int[150,4]; // clusters array
	private float[,] averagedata = new float[150, 2];


	// Links
	private FunnelIO datafunnel;
	private TrackMaker trackmaker; 

	//Placement Objects
	public GameObject Selector;
	private GameObject marker;
	private GameObject[] hitmarkers;
	public Transform toruspos;
	private string tagger;

	// UI
	public Text noisypixels;
	public Text newpixels;
	public Text ErrorMessage;
	public GameObject ErrorSection;


	void Start () {

		//****** Initialisations
		record = false; // choose whether or not to save clusters
		done = false; // signals when data processing is ready
		numbatches = 0;// keeps track of no. of data bundles analyzed

		// Links to main scripts
		datafunnel = GameObject.Find ("Main Camera").GetComponent<FunnelIO>();//links to the data provider
		trackmaker = GameObject.Find ("Main Camera").GetComponent<TrackMaker>();//links to the data provider


		if (planeno == 0) { // taggers for hit markers
			tagger = "TorusA";
		} else if (planeno == 1) {
			tagger = "TorusB";
		} else if (planeno == 2) {
			tagger = "TorusC";
		} else {
			tagger = "TorusD";
		}

		//******** creating links required for positioning torus and tracks etc
		trackmaker.datapos [planeno].position = transform.position;
		trackmaker.datapos [planeno].rotation = transform.rotation;
		trackmaker.inittranspoint [planeno]   = transform.position;
		toruspos.rotation = transform.rotation;

		// ******* Starting text
	    noisypixels.text = " ";
		newpixels.text = " ";


		// ******* Setting up Data Paths
		FilePathNoise = getPath(0);
		FilePathData = getPath (1);
		FilePathClusters = getPath (2);
		dataStream = System.IO.File.CreateText(FilePathData);
		clusterStream = System.IO.File.CreateText(FilePathClusters);

	}

	void FixedUpdate() //repeated every 0.02s currently
	{
		if (applydata == true) {// user needs to press the correct button to start data flow
				
			if (planeno == 3) {
	//			Debug.Log ("Hi");
			}
				if (datafunnel.datavalid [planeno] == true) { //data is valid, calculations can begin
					datafunnel.datavalid [planeno] = false; // only once
					done = false;// makes sure the done trigger is off

					if (measuring == true) { //user selects between measuring data/measuring noise
						
						if (maskready == true) { //if the mask is loaded and ready, one can simply start reading
							trackmaker.triggers [planeno] = false;//makes sure track triggers are off
							Gather (); //main gathering function
						} 
						else {// otherwise, the mask needs to be read from the stored file
							StartCoroutine (ReadData ());
						}
						done = true;//signals calculations are ready

						// implement here something to prevent user from pressing measure without a noise mask
					} 
					else {// searches for noise
						NoiseFilter ();
						done = true;// signals calculations are ready

					}
				}
		}
	}

	void NoiseFilter() //function which extracts noise etc
	{
		if (Data != null) {
			if (testnum < maskchecks) { // keeps on going here until the set number of masks is reached
				for (int y = 0; y < Data.nhits; y = y + 2) { // checks for noise here
					CheckNoise (Data.hits [y], Data.hits [y + 1]);
				}
				testnum += 1;
				if (planeno == 0) {
					noisypixels.text = "Noise Event: " + testnum.ToString ();
				} else {
					noisypixels.text = " ";
				}
			} 
            else {
				AssignMask ();//with all the data ready, find the mask
				noisypixels.text = "Mask Plane "+ planeno.ToString() +": " + num_mask.ToString ();
				maskready = true;// mask is now ready
			}
         
		}

	}
		
	void AssignMask()
	{
		for (int i = 0; i < position; i++) {
			if (repeatedreadings [i, 2] > maskthreshold) { //if pixels are hit more than a specific amount, mask

				mask [num_mask, 0] = repeatedreadings [i, 0];
				mask [num_mask, 1] = repeatedreadings [i, 1];

				num_mask = num_mask + 1; //keeps a count of the number of masked pixels

			}
		}
		applydata = false; // stop reading

		datafunnel.shutoff[planeno] = true; //turn menu back on and stop data stream

		type = 0;//noise saving routine
		StartCoroutine (SaveData ());

	}
	void CheckNoise(int datax,int datay)
	{
		bool repeated = false;
		for (int i = 0; i < position; i++) {//if repeated, inc count
			if (repeatedreadings [i, 0] == datax && repeatedreadings[i,1] == datay) {
				repeated = true;
				repeatedreadings [i, 2] = repeatedreadings [i, 2] + 1;
				break;
			}
		}
		if (repeated == false) {// if not repeated, add a new position
			repeatedreadings [position, 0] = datax;
			repeatedreadings [position, 1] = datay;
			repeatedreadings [position, 2] = repeatedreadings [position, 2] + 1;
			position = position + 1;
		}
	}
	void WipeTex()
	{
		// destroy all hit markers when refreshing
			hitmarkers = GameObject.FindGameObjectsWithTag (tagger);
			foreach (GameObject torus in hitmarkers)
			{
				Destroy (torus);
			}		
	}
		
	void Gather()
	{
		numbatches++; // new batch
		WipeTex (); // remove previous marks
	
		Demasking (); // demask new data and find clusters

		TriggerTracks (); //check if track formation should be triggered

		MarkHit ();		//marks out texture/assigns torus

		newpixels.text = "Hits Plane "+ planeno.ToString() +": " + bluepix.ToString (); // updates Stats
		bluepix = 0;

		noisypixels.text = "Mask Plane "+ planeno.ToString() +": " + num_mask.ToString ();// updates noise stats

	}

	void Demasking()
	{
		numnonmasked = 0;
		DemaskedData.nhits = Data.nhits;
		DemaskedData.hits = new int[Data.nhits];
		for (int y = 0; y < Data.nhits; y = y + 2) {
			
			for (int i = 0; i < num_mask; i++) {// remove masked pixels from input data
				try
				{
					if (Data.hits [y] == mask [i, 0] && Data.hits [y + 1] == mask [i, 1]) {
						DemaskedData.nhits -= 2;
						masked = 1;
						break;
					} 
				}
				catch{
					//Debug.Log("Data error: " + y + " MaxData: " + Data.hits.Length);
					//Debug.Log ("JSON IN:" + datafunnel.datarray [planeno]);
				}
			}
			if (masked == 0) {
				try
				{
					DemaskedData.hits [numnonmasked] = Data.hits [y];
					DemaskedData.hits [numnonmasked + 1] = Data.hits [y + 1];
				}
				catch {
				//	Debug.Log("Demask error: " + y + " MaxDemask: " + DemaskedData.hits.Length);
				//	Debug.Log ("JSON IN:" + datafunnel.datarray [planeno]);
				}
				numnonmasked += 2;
			} else {
				masked = 0;
			}

		}
		type = 1;// raw data save
		StartCoroutine (SaveData ());
	
		clustersfound = FindClusters ();// find clusters and their average position
	    AverageCluster();
		type = 2; //cluster save
		if (record == true) {
			StartCoroutine (SaveData ());
		}
			
	}

	void TriggerTracks()
	{// assign data to track maker script if there are enough hits
		if ( DemaskedData.nhits > 0) {
			trackmaker.triggers [planeno] = true;

			if (planeno == 0) { //not as efficient as possible
				trackmaker.av0 = averagedata;
			} else if (planeno == 1) {
				trackmaker.av1 = averagedata;

			} else if (planeno == 2) {
				trackmaker.av2 = averagedata;
			} else if (planeno == 3) {
				trackmaker.av3 = averagedata;
			}

			trackmaker.clusters [planeno] = clustertotal;

		} else {
			trackmaker.triggers [planeno] = false;
		}
	}

	void MarkHit()
	{
		// mark a hit using a torus marker
		for (int y = 0; y < DemaskedData.nhits; y = y + 2) {
			toruspos.position = transform.position - new Vector3 (1000, 0, 500) + new Vector3 (DemaskedData.hits [y] * dividerx, 0.0f, DemaskedData.hits [y + 1] * dividerz) + new Vector3 (0.5f * dividerx, 0, 0.5f * dividerz);
			marker = Instantiate (Selector, toruspos.position, transform.rotation);
			marker.tag = tagger;
			bluepix = bluepix + 1;
		}
	}
	private int[,] FindClusters()
	{
		nextentry = 0;
		int[,] clusters = new int[150,4];

		int currentcluster = 1;
		int clusterstartpoint = 0;
		bool pixelok = false;

		for (int k = 0; k < DemaskedData.nhits; k = k+2) {

			for (int u = clusterstartpoint; u < nextentry; u++) {

				if (clusters [u, 3] == k) {   // checking if the pixel has already been assigned a cluster
					pixelok = true;
					break;

				}

			}
			if (pixelok == false) {

				clusters[nextentry,0] = DemaskedData.hits[k]; 
				clusters[nextentry,1] = DemaskedData.hits[k+1];
				clusters[nextentry,2] = currentcluster;
				clusters[nextentry,3] = k;
				nextentry += 1;
				clusters = FindAdjacents (clusters, currentcluster, clusterstartpoint, k, k);
				currentcluster +=1;
			} else {
				pixelok = false;
			}
		}
		clustertotal = currentcluster-1;
		return clusters;
	}
		
	private int[,] FindAdjacents(int[,]clusters,int currentcluster,int clusterstartpoint, int basepixel, int currentpixel)
	{

		int currentadjacents = 0;
		int tempx = DemaskedData.hits[currentpixel];  //assign data for the pixel under consideration
		int tempy = DemaskedData.hits[currentpixel+1];
		bool pixelok = false;
		for (int i =basepixel+2; i < DemaskedData.nhits; i = i + 2) {//going through pixel (can ignore pixels underneath the base pixel i.e. first pixel in cluster

			for (int c = clusterstartpoint; c < nextentry; c++) {

				if (clusters [c, 3] == i) {   // checking if the pixel has already been assigned a cluster
					pixelok = true;

					break;
				}

			}
			if (pixelok == false) { // if new pixel doesn't already have a cluster
				if (DemaskedData.hits [i] == tempx) { // checks if pixel is vertically adjacent

					if ((DemaskedData.hits [i + 1] == tempy + 1) || (DemaskedData.hits [i + 1] == tempy - 1)) {
						clusters [nextentry, 0] = DemaskedData.hits [i]; //enter data of this pixel into the main cluster array
						clusters [nextentry, 1] = DemaskedData.hits [i + 1];
						clusters [nextentry, 2] = currentcluster; // mark this pixel with its cluster
						clusters [nextentry, 3] = i; // mark this pixel's index entry (for use when ignoring already set pixels)
						nextentry += 1;//jump to the next entry
						clusters = FindAdjacents (clusters, currentcluster, clusterstartpoint, basepixel, i);// repeat until pixels have no more adjacents
						currentadjacents += 1; //increment num of adjacents

					}

				} else if (DemaskedData.hits [i + 1] == tempy) {//repeat for horizontally adjacent pixels
					if ((DemaskedData.hits [i] == tempx + 1) || (DemaskedData.hits [i] == tempx - 1)) {


						clusters [nextentry, 0] = DemaskedData.hits [i];
						clusters [nextentry, 1] = DemaskedData.hits [i + 1];
						clusters [nextentry, 2] = currentcluster; 
						clusters [nextentry, 3] = i;
						nextentry += 1;
						clusters = FindAdjacents (clusters, currentcluster, clusterstartpoint, basepixel, i);
						currentadjacents += 1; 

					}
				}

				if (currentadjacents == 4) { // can only ever be four adjacent pixels to a single pixel
					break;
				}
			} else {
				pixelok = false;
			}

		}
		currentadjacents = 0;

		return clusters;
	}

	void AverageCluster()
	{
		int i = 0;
		int totalx = 0;
		int totaly = 0;
		int startpos = 0;
		int diff = 1;

		for (int j =0; j<clustertotal;j++ )
		{
			totalx = clustersfound [i, 0];
			totaly = clustersfound [i, 1];
			startpos = i;
		while (clustersfound [i,2] == clustersfound [i + 1,2]) {

			totalx = totalx + clustersfound [i+1, 0];
			totaly = totaly + clustersfound [i+1, 1];
			i++;
		}
			i++;

			if (i > startpos) {
				diff = i - startpos;
			} else {
				diff = 1;
			}
		
			averagedata [j, 0] =  (totalx*1.0f)/ diff; // check rounding

			averagedata [j, 1] = totaly / diff;

		}

	}
	private IEnumerator SaveData()
	{
		string[] rowDataTemp;
		// Creating First row of titles manually..

		if (type == 0) {
			rowDataTemp = new string[2];
			noiseStream = System.IO.File.CreateText (FilePathNoise);
			rowDataTemp [0] = "Plane " + planeno + " Mask Data";
			rowDataTemp [1] = "" + num_mask;
			rowData.Add (rowDataTemp);
		}

		if (type == 0) {
			for (int i = 0; i < num_mask; i++) {
				rowDataTemp = new string[2];
				rowDataTemp [0] = "" + mask [i, 0]; 
				rowDataTemp [1] = "" + mask [i, 1];
				rowData.Add (rowDataTemp);
			}

		} else if (type == 1) {
			for (int i = 0; i < Data.nhits; i = i + 2) { //change to demasked later

				rowDataTemp = new string[2];

				rowDataTemp [0] = "" + Data.hits [i]; 
				rowDataTemp [1] = "" + Data.hits [i + 1];

				rowData.Add (rowDataTemp);
			}

		} else {
			for (int i = 0; i < nextentry; i++) {

				rowDataTemp = new string[5];

				rowDataTemp [0] = "Event " + numbatches; 
				rowDataTemp [1] = "Cluster ID " + clustersfound[i,2]; 
				rowDataTemp [2] = "Xcoord: " + clustersfound [i, 0];
				rowDataTemp [3] = "Ycoord: " + clustersfound [i, 1];
				rowDataTemp [4] = "Date/Time: " + System.DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
				rowData.Add (rowDataTemp);
			}
			if (nextentry == 0) {
				rowDataTemp = new string[5];
				rowDataTemp [0] = "Event " + numbatches; 
				rowDataTemp [1] = "No Hits";
				rowDataTemp [2] = "No Hits";
				rowDataTemp [3] = "No Hits";
				rowDataTemp [4] = "Date/Time: " + System.DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
				rowData.Add (rowDataTemp);

			}
		}
		string[][] output = new string[rowData.Count][];

		for(int i = 0; i < output.Length; i++){
			output[i] = rowData[i];
		}

		int     length         = output.GetLength(0);
		string  delimiter     = ",";

		StringBuilder sb = new StringBuilder();

		for (int index = 0; index < length; index++)
			sb.AppendLine(string.Join(delimiter, output[index]));

		if (sb.Length != 0) {
			sb.Remove (sb.Length - 1, 1);
		}

		if (type == 0) {
			noiseStream.WriteLine (sb);
			noiseStream.Close ();
		} else if (type == 1) {
			if (sb.Length != 0) {
				dataStream.WriteLine (sb);
			}

		} else {
			clusterStream.WriteLine (sb);
		}
		rowData.Clear();
		yield return null;
	}

	private IEnumerator ReadData()
	{
		try{
			StreamReader inStream = new StreamReader (FilePathNoise);
			string[] s = inStream.ReadLine ().Split (',');
			num_mask = Convert.ToInt32 (s [1]); //getting total number of masked pixels

			for (int i =0; i < num_mask ;i++) //obtaining mask_data
			{
				s = inStream.ReadLine ().Split (',');
				mask [i, 0] = Convert.ToInt32 (s [0]);
				mask [i, 1] = Convert.ToInt32 (s [1]);
			}

			inStream.Close ();
			maskready = true;

		}
		catch
		{
			datafunnel.startdata = false;
			applydata = false;
			ErrorMessage.text = "Mask files absent";
			ErrorSection.SetActive (true);
		}

		yield return null;
	}
	private string getPath(int mode){


		if (mode == 0) {
			#if UNITY_EDITOR
			return Application.dataPath + "/CSV/Mask/" + "Saved_mask_plane_" + planeno + ".csv";
			#else
			return Application.dataPath +"/"+"Saved_mask_plane_"+planeno+".csv";
			#endif
		} else if (mode == 1) {
			#if UNITY_EDITOR
			return Application.dataPath + "/CSV/Data/" + "Data_Stream_" + planeno + ".csv";
			#else
			return Application.dataPath +"/"+"Data_Stream_"+planeno+".csv";
			#endif

		} else {

			#if UNITY_EDITOR
			return Application.dataPath + "/CSV/Data/" + "Cluster_Stream_" + planeno + ".csv";
			#else
			return Application.dataPath +"/"+"Cluster_Stream_"+planeno+".csv";
			#endif
		}

	}
}


	


