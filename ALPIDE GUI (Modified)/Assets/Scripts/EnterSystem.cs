﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnterSystem : MonoBehaviour {
	public GameObject[] planes;
	public bool functionality = false;
	public GameObject ErrorMessage;
	public Text Errortext;
	private FunnelIO DataFunnel;
	public InputField NoiseEvents;
	public InputField NoiseThreshold;
	public InputField[] H;
	private TrackMaker Trackchecker;
	private int i = 0;

	void Start()
	{
	//	planes = GameObject.FindGameObjectsWithTag ("Plane");
		DataFunnel = Camera.main.GetComponent<FunnelIO> ();
		Trackchecker = Camera.main.GetComponent<TrackMaker> ();
	}
		
	public void StartMeasure()
	{

		if (DataFunnel.portfound == true) {
			
			DataFunnel.startdata = true;

			for (i = 0; i < 3; i++) {
				Trackchecker.Heights [i] = float.Parse(H[i].text);
			}


			foreach (GameObject Plane in planes) {

				Plane.GetComponent<TileMap> ().applydata = true;
				Plane.GetComponent<TileMap> ().measuring = functionality;
				Plane.GetComponent<TileMap> ().maskchecks = int.Parse(NoiseEvents.text);
				Plane.GetComponent<TileMap> ().maskthreshold = int.Parse(NoiseThreshold.text);
			}	
		} 
		else {

			ErrorMessage.SetActive (true);
			Errortext.text = "Connect ALPIDE Stack";
		}
	}
}