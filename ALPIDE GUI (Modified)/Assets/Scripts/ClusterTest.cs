﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClusterTest : MonoBehaviour {
	private int nextentry = 1;
	int[,] clustersout = new int[80,4];
	//string json = "{\"nhits\":16,\"hits\":[5,0,5,1,3,2,489,393,503,366,552,82,490,393,583,346,489,395,685,220,489,396,894,432,942,406,949,28,489,394,1016,87]}";
	string json = "{\"nhits\":20,\"hits\":[668,318,701,284,708,270,723,186,725,502,724,434,724,470,726,402,727,416,726,434,727,409,730,412,731,419,737,423,737,422,737,418,736,419,744,431,767,461,50,63]}";
	// Use this for initialization
	public class PixelHits // takes care of JSON input
	{
		public int nhits;
		public int[] hits;
	}
	private PixelHits Data = new PixelHits();
	void Start () {
		Data = JsonUtility.FromJson<PixelHits>(json);//collect hit data
		Data.nhits = Data.nhits*2;
		clustersout = FindClustersx();
		for (int i = 0; i < nextentry; i++) {
			try{
				Debug.Log (clustersout[i,0] + " " + clustersout[i,1] +" "+ clustersout[i,2] +" "+ clustersout[i,3]);
			}
			catch {
				Debug.LogError ("Length : "+clustersout.Length + " wrong index: " + i);
			}
		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private int[,] FindClustersx()
	{
		nextentry = 0;
		int[,] clusters = new int[80,4];

		int currentcluster = 1;
		int clusterstartpoint = 0;
		bool pixelok = false;

		for (int k = 0; k < Data.nhits; k = k+2) {

			for (int u = clusterstartpoint; u < nextentry; u++) {

				if (clusters [u, 3] == k) {   // checking if the pixel has already been assigned a cluster
					pixelok = true;
					break;
				}

			}
			if (pixelok == false) {

				clusters[nextentry,0] = Data.hits[k]; 
				clusters[nextentry,1] = Data.hits[k+1];
				clusters[nextentry,2] = currentcluster;
				clusters[nextentry,3] = k;
				nextentry += 1;
				clusters = FindAdjacents (clusters, currentcluster, clusterstartpoint, k, k);
				currentcluster +=1;
			} else {
				pixelok = false;
			}
		}

		return clusters;
	}


	private int[,] FindAdjacents(int[,]clusters,int currentcluster,int clusterstartpoint, int basepixel, int currentpixel)
	{

		int currentadjacents = 0;
		int tempx = Data.hits[currentpixel];  //assign data for the pixel under consideration
		int tempy = Data.hits[currentpixel+1];
		bool pixelok = false;
		for (int i =basepixel+2; i < Data.nhits; i = i + 2) {//going through pixel (can ignore pixels underneath the base pixel i.e. first pixel in cluster

			for (int c = clusterstartpoint; c < nextentry; c++) {

				if (clusters [c, 3] == i) {   // checking if the pixel has already been assigned a cluster
					pixelok = true;
					break;
				}

			}
			if (pixelok == false) { // if new pixel doesn't already have a cluster
				if (Data.hits [i] == tempx) { // checks if pixel is vertically adjacent

					if ((Data.hits [i + 1] == tempy + 1) || (Data.hits [i + 1] == tempy - 1)) {
						clusters [nextentry, 0] = Data.hits [i]; //enter data of this pixel into the main cluster array
						clusters [nextentry, 1] = Data.hits [i + 1];
						clusters [nextentry, 2] = currentcluster; // mark this pixel with its cluster
						clusters [nextentry, 3] = i; // mark this pixel's index entry (for use when ignoring already set pixels)
						nextentry += 1;//jump to the next entry
						clusters = FindAdjacents (clusters, currentcluster, clusterstartpoint, basepixel, i);// repeat until pixels have no more adjacents
						currentadjacents += 1; //increment num of adjacents

					}

				} else if (Data.hits [i + 1] == tempy) {//repeat for horizontally adjacent pixels
					if ((Data.hits [i] == tempx + 1) || (Data.hits [i] == tempx - 1)) {


						clusters [nextentry, 0] = Data.hits [i];
						clusters [nextentry, 1] = Data.hits [i + 1];
						clusters [nextentry, 2] = currentcluster; 
						clusters [nextentry, 3] = i;
						nextentry += 1;
						clusters = FindAdjacents (clusters, currentcluster, clusterstartpoint, basepixel, i);
						currentadjacents += 1; 

					}
				}

				if (currentadjacents == 4) { // can only ever be four adjacent pixels to a single pixel
					break;
				}
			} else {
				pixelok = false;
			}

		}
		currentadjacents = 0;

		return clusters;
	}

}
