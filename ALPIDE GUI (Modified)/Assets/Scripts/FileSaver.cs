﻿/***************************************************************************
*  ALPIDE Visualiser - Companion Software to ALPIDE Arduino Library        *
*  Copyright (C) 2017 Matthew Aquilina <matthewaq31@gmail.com>             *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using System;

public class FileSaver : MonoBehaviour {

	private List<string[]> rowData = new List<string[]>();
	private PixelHits Data = new PixelHits();//data obtained from Arduino stream

	private IEnumerator datasaver;

	// Use this for initialization
	public class PixelHits // takes care of JSON input
	{
		public int nhits;
		public int[] hits;
	}


	void Start () {
			string json = "{\"nhits\":16,\"hits\":[5,59,5,1,3,2,489,393,503,366,552,82,569,22,583,346,624,81,685,220,710,460,894,432,942,406,949,28,958,397,1016,87]}";

			Data = JsonUtility.FromJson<PixelHits>(json);
		    Data.nhits = Data.nhits * 2;
		datasaver = SaveData ();
		StartCoroutine (datasaver);
	}

	private IEnumerator SaveData()
	{

		// Creating First row of titles manually..
		string[] rowDataTemp = new string[2];
		rowDataTemp[0] = "Nhits(multiplied)";
		rowDataTemp[1] = "" + Data.nhits;
		rowData.Add(rowDataTemp);

		// You can add up the values in as many cells as you want.
		for(int i = 0; i < Data.nhits; i+=2){
			rowDataTemp = new string[2];
			rowDataTemp[0] = "" + Data.hits[i]; // name
			rowDataTemp[1] = ""+Data.hits[i+1]; // ID
			rowData.Add(rowDataTemp);
		}

		string[][] output = new string[rowData.Count][];

		for(int i = 0; i < output.Length; i++){
			output[i] = rowData[i];
		}

		int     length         = output.GetLength(0);
		string     delimiter     = ",";

		StringBuilder sb = new StringBuilder();

		for (int index = 0; index < length; index++)
			sb.AppendLine(string.Join(delimiter, output[index]));


		string filePath = getPath();

		StreamWriter outStream = System.IO.File.CreateText(filePath);
		outStream.WriteLine(sb);
		outStream.Close();

		StreamReader inStream = new StreamReader (filePath);
		inStream.Close ();

		yield return null;
	}

	private string getPath(){
		#if UNITY_EDITOR
		return Application.dataPath +"/CSV/"+"Saved_data.csv";
		#else
		return Application.dataPath +"/"+"Saved_data.csv";
		#endif
		}
}
