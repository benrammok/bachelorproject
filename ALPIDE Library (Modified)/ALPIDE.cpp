/***************************************************************************
*  ALPIDE - Arduino reference library for the ALPIDE shield.               *
*  Copyright (C) 2017 Magnus Mager <Magnus.Mager@cern.ch>                  *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/

#include <Arduino.h>
#include "ALPIDE.h"

#define PLANE2ID(plane) (7<<4|(plane)<<2|3) //This might change
#define ID2PLANE(id) (((id)&0x3)==0x3?(id)>>2:-1) /* this is for 4-bit chipIDs */
#define DEBUG false

ALPIDE::ALPIDE() {
  digitalWrite(ALPIDE_PIN_PWR_EN,LOW);
  digitalWrite(ALPIDE_PIN_CLK   ,LOW);
  digitalWrite(ALPIDE_PIN_RST_N ,LOW);
  digitalWrite(ALPIDE_PIN_CTRL  ,LOW);
  pinMode(ALPIDE_PIN_PWR_EN,OUTPUT);
  pinMode(ALPIDE_PIN_RST_N ,OUTPUT);
  pinMode(ALPIDE_PIN_CLK   ,OUTPUT);
  pinMode(ALPIDE_PIN_CTRL  ,OUTPUT);
}

void ALPIDE::power(bool on) {
  if (on) {
    digitalWrite(ALPIDE_PIN_CLK   ,LOW);
    digitalWrite(ALPIDE_PIN_RST_N ,LOW);
    digitalWrite(ALPIDE_PIN_CTRL  ,LOW);
    pinMode(ALPIDE_PIN_CTRL,OUTPUT);
    digitalWrite(ALPIDE_PIN_PWR_EN,HIGH);
    digitalWrite(ALPIDE_PIN_CTRL  ,HIGH);
    digitalWrite(ALPIDE_PIN_RST_N ,HIGH);
    delay(100); // TODO: how long does it take to stabilise?
  }
  else {
    digitalWrite(ALPIDE_PIN_CLK   ,LOW);
    digitalWrite(ALPIDE_PIN_RST_N ,LOW);
    digitalWrite(ALPIDE_PIN_CTRL  ,LOW);
    pinMode(ALPIDE_PIN_CTRL,OUTPUT);
    digitalWrite(ALPIDE_PIN_PWR_EN,LOW);
  }
}

int ALPIDE::init() {
  digitalWrite(ALPIDE_PIN_RST_N,LOW );
  digitalWrite(ALPIDE_PIN_RST_N,HIGH);
  clk(100); // TODO: needed?
  broadcastCommand(ALPIDE_CMD_GRST);
  broadcastCommand(ALPIDE_CMD_PRST);
  broadcastWriteRegister(ALPIDE_REG_CMUDMU_CFG,0x0018); // disable manchester encoding, have initial token
  broadcastWriteRegister(ALPIDE_REG_FROMU_CFG1,0x0000); // disable busy monitoring
  broadcastWriteRegister(ALPIDE_REG_FROMU_CFG2,0x0000); // one clock cycle strobe
  broadcastWriteRegister(ALPIDE_REG_DAC_VRESETD,APIDE_DEFAULT_VRESETD);
  //broadcastWriteRegister(0x0604,0x3C); //VCASN
  //broadcastWriteRegister(0x060E,30); // ITHR
  // disable all masking
  broadcastWriteRegister(0x0487,0x0000);
  broadcastWriteRegister(0x0500,0x0000);
  broadcastWriteRegister(0x0487,0xFFFF);
  broadcastWriteRegister(0x0487,0x0000);
  broadcastWriteRegister(ALPIDE_REG_MODE_CTRL,0x0209); // triggered readout, no clustering, fast matrix readout, no skewing, not cluck gating, read from CMU
  broadcastCommand(ALPIDE_CMD_RORST);
  clk(100); // TODO: some are needed, but how many? TODO: check again!
  return 0;
}

int ALPIDE::trigger(int strobeMilliSeconds) {
  // start bit
  digitalWrite(ALPIDE_PIN_CTRL,LOW );
  pinMode(ALPIDE_PIN_CTRL,OUTPUT);
  clk();
  // trigger bits (0x55, alternatives: 0xB1,0xC9,0x2D, or not caring about hamming: 0bxxxxxx01), LSB first
  digitalWrite(ALPIDE_PIN_CTRL,HIGH);clk();
  digitalWrite(ALPIDE_PIN_CTRL,LOW );clk();
  digitalWrite(ALPIDE_PIN_CTRL,HIGH);clk();
  digitalWrite(ALPIDE_PIN_CTRL,LOW );clk();
  digitalWrite(ALPIDE_PIN_CTRL,HIGH);clk();
  digitalWrite(ALPIDE_PIN_CTRL,LOW );clk();
  delay(strobeMilliSeconds);
  digitalWrite(ALPIDE_PIN_CTRL,HIGH);clk();
  digitalWrite(ALPIDE_PIN_CTRL,LOW );clk();
  digitalWrite(ALPIDE_PIN_CTRL,HIGH);clk();
  return 0;
}

int ALPIDE::readRawData(uint8_t plane,uint16_t *buf,int n) {
  int i=0;
  while (n>0) {
    int32_t lo=readRegister(plane,ALPIDE_REG_DMU_FIFO_LO);
    int32_t hi=readRegister(plane,ALPIDE_REG_DMU_FIFO_HI);
    if (lo<0) return -1;
    if (hi<0) return -2;
    if (lo&0x00FF!=0x00FF) return -3;
    if (hi==0xFF && lo==0xFFFF) break;
    buf[i++]=hi<<8|lo>>8;
    --n;
  }
  return i;
}

int ALPIDE::readEvent(uint8_t plane,uint16_t *x,uint16_t *y,int n) {
  int type;
  int8_t   currentRegion=-1;
  uint16_t xread           ;
  uint16_t yread           ;
  uint8_t  bc              ;
  int8_t   planeread       ;
  uint8_t  flags           ;
  uint16_t rawdata;
  int ret=readRawData(plane,&rawdata,1);
  if (ret==0) return -10;
  if (ret<0) return -100+ret;
  type=decodeRawDataWord(rawdata,&currentRegion,&xread,&yread,&bc,&planeread,&flags);
  
  if(DEBUG){
  Serial.print("Type");
  Serial.println(type);
  Serial.print("DATA CHIP HEADER: ");
  Serial.println(ALPIDE_DATA_CHIP_HEADER);
  Serial.print("EMPTY CHIP HEADER: ");
  Serial.println(ALPIDE_DATA_CHIP_EMPTY);
  Serial.print("Plane: ");
  Serial.println(plane);
  
  Serial.print("Recieved ID: ");
  Serial.println(planeread);
  }
  
  if (!(type==ALPIDE_DATA_CHIP_HEADER || type==ALPIDE_DATA_CHIP_EMPTY)) return -1;

  if (planeread!=plane) return -2;
  if (type==ALPIDE_DATA_CHIP_EMPTY) return 0;
  int i=0;
  while (true) {
    ret=readRawData(plane,&rawdata,1);
    if (ret<0) return -1;
    if (ret==0) return -10;
    type=decodeRawDataWord(rawdata,&currentRegion,&xread,&yread,&bc,&planeread,&flags);
    switch (type) {
      case ALPIDE_DATA_REGION_HEADER:
        break;
      case ALPIDE_DATA_DATA_SHORT:
        if (n==0) return -3;
        x[i]=xread;
        y[i]=yread;
        ++i;
        --n;
        break;
      case ALPIDE_DATA_CHIP_TRAILER:
        //if (flags!=0) return -200-flags;
        return i;
      default:
        return -5;
    }
  }
}

int ALPIDE::writeRegister(uint8_t plane,uint16_t addr,uint16_t data) {
  writeByte(ALPIDE_CMD_WROP);
  writeByte(PLANE2ID(plane));
  for (int i=0;i<2;++i) writeByte(addr>>(i*8)&0xFF);
  for (int i=0;i<2;++i) writeByte(data>>(i*8)&0xFF);
  clk(10); // TODO: at least writing to DACs need some clocks to be effective...
  return 0;
}

int32_t ALPIDE::readRegister(uint8_t plane,uint16_t addr) {
  writeByte(ALPIDE_CMD_RDOP);
  writeByte(PLANE2ID(plane));
  for (int i=0;i<2;++i) writeByte(addr>>(i*8)&0xFF);
  // bus turn around:
  clk(5);                   // -  0- 4: 5 cycles IDLE driven by master
  pinMode(ALPIDE_PIN_CTRL,INPUT);
  // TODO: double check this timing with chip designers. Number in the code seem to work...
  clk(5);                   // -  5- 9: 5 cycles with high impeance, ingnoring the bus
  bool idle1 =checkIdle(4); // - 10-14: 5 extra IDLE driven by slave
  int  chipid=readByte();   // - 15-24: chip ID character
  bool idle2 =checkIdle(0); // - 25-25: 1 extra IDLE driven by slave
  int  datal =readByte();   // - 26-35: data low character
  bool idle3 =checkIdle(0); // - 36-37: 2 extra IDLE driven by slave
  int  datah =readByte();   // - 38-47: data high character
  bool idle4 =checkIdle(5); // - 48-52: 5 extra IDLE driven by slave
  clk(4);                   // - 53-56: 4 cycles ingore
  digitalWrite(ALPIDE_PIN_CTRL,HIGH);
  pinMode(ALPIDE_PIN_CTRL,OUTPUT);
  clk(8);                   // - 57-64: 8 cycles IDLE driven by master
  if (!idle1)  return -1;
  if (chipid!=PLANE2ID(plane)) return -5;
  if (!idle2)  return -2;
  if (datal<0) return -6;
  if (!idle3)  return -3;
  if (datah<0) return -7;
  if (!idle4)  return -4;
  return (uint16_t)datah<<8|datal;
}

int ALPIDE::broadcastCommand(uint8_t cmd) {
  writeByte(cmd);
  return 0;
}

int ALPIDE::broadcastWriteRegister(uint16_t addr,uint16_t data) {
  writeByte(ALPIDE_CMD_WROP);
  writeByte(0x0F);
  for (int i=0;i<2;++i)
    writeByte(addr>>(i*8)&0xFF);
  for (int i=0;i<2;++i)
    writeByte(data>>(i*8)&0xFF);
  return 0;
}

void ALPIDE::writeByte(uint8_t data) {
  // start bit
  digitalWrite(ALPIDE_PIN_CTRL,LOW );
  pinMode(ALPIDE_PIN_CTRL,OUTPUT);
  clk();
  // data bits, LSB first
  for (int i=0;i<8;++i) {
    digitalWrite(ALPIDE_PIN_CTRL,data>>i&0x1);
    clk();
  }
  // stop bit
  digitalWrite(ALPIDE_PIN_CTRL,HIGH);
  clk();
}

int16_t ALPIDE::readByte() {
  pinMode(ALPIDE_PIN_CTRL,INPUT);
  // stop bit
  clk();
  bool startfound=(digitalRead(ALPIDE_PIN_CTRL)==LOW );
  uint8_t data=0;
  // data bits, LSB first
  for (int i=0;i<8;++i) {
    clk();
    data>>=1;
    if (digitalRead(ALPIDE_PIN_CTRL)==HIGH) data|=0x80; 
  }
  // stop bit
  clk();
  bool stopfound =(digitalRead(ALPIDE_PIN_CTRL)==HIGH);
  if (!startfound) return -1;
  if (!stopfound ) return -2;
  return data;
}

int ALPIDE::checkIdle(int n) {
  pinMode(ALPIDE_PIN_CTRL,INPUT);
  bool idle=true;
  for (int i=0;i<n;++i) {
    clk();
    if (digitalRead(ALPIDE_PIN_CTRL)!=HIGH) idle=false;
  }
  return idle;
}

void ALPIDE::clk(int n) {
  for (int i=0;i<n;++i) {
    digitalWrite(ALPIDE_PIN_CLK,HIGH);
    digitalWrite(ALPIDE_PIN_CLK,LOW );
  }
}

int ALPIDE::decodeRawDataWord(uint16_t word,int8_t *currentRegion,uint16_t *x,uint16_t *y,uint8_t *bc,int8_t *plane,uint8_t *flags) {
  if      ((word&0xF000)==0xA000) {
    *bc           =word&0xFF;
    *plane        =ID2PLANE(word>>8&0x0F);
    *currentRegion=-1;
    return ALPIDE_DATA_CHIP_HEADER;
  }
  else if ((word&0xF0FF)==0xB0FF) {
    *currentRegion=-1;
    *flags=word>>8&0xF;
    return ALPIDE_DATA_CHIP_TRAILER;
  }
  else if ((word&0xF000)==0xE000) {
    *bc           =word&0xFF;
    *plane        =ID2PLANE(word>>8&0x0F);
    *currentRegion=-1;
    return ALPIDE_DATA_CHIP_EMPTY;
  }
  else if ((word&0xE0FF)==0xC0FF) {
    *currentRegion=word>>8&0x1F;
    return ALPIDE_DATA_REGION_HEADER;
  }
  else if ((word&0xC000)==0x4000) {
    *x=*currentRegion<<5|word>>9&0x1E|(word^word>>1)&0x1;
    *y=word>>1&0x1FF;
    return ALPIDE_DATA_DATA_SHORT;
  }
  // TODO: will busy transactions appear and will they be reported?
  else if (word       ==0xF1FF) {
    return ALPIDE_DATA_BUSY_ON;
  }
  else if (word       ==0xF0FF) {
    return ALPIDE_DATA_BUSY_OFF;
  }
  else {
    return -1;
  }
}

void ALPIDE::maskAllPixels(uint8_t plane,bool enable) {
  writeRegister(plane,0x0487,0x0000);
  writeRegister(plane,0x0500,enable?0x0002:0x0000);
  writeRegister(plane,0x0487,0xFFFF);
  writeRegister(plane,0x0487,0x0000);
}

void ALPIDE::enablePulserAllPixels(uint8_t plane,bool enable) {
  writeRegister(plane,0x0487,0x0000);
  writeRegister(plane,0x0500,enable?0x0003:0x0001);
  writeRegister(plane,0x0487,0xFFFF);
  writeRegister(plane,0x0487,0x0000);
}

void ALPIDE::maskPixel(uint8_t plane,uint16_t x,uint16_t y,bool enable) {
  writeRegister(plane,0x0487,0x0000);
  writeRegister(plane,0x0500,enable?0x0002:0x0000);
  writeRegister(plane,(x&0x3E0)<<6|0x0400|(1+(x>>4&0x1)),1<<(x&0xF));
  writeRegister(plane,(y&0x1F0)<<7|0x0404               ,1<<(y&0xF));
  writeRegister(plane,0x0487,0x0000);
}

void ALPIDE::enablePulserPixel(uint8_t plane,uint16_t x,uint16_t y,bool enable) {
  writeRegister(plane,0x0487,0x0000);
  writeRegister(plane,0x0500,enable?0x0003:0x0001);
  writeRegister(plane,(x&0x3E0)<<6|0x0400|(1+(x>>4&0x1)),1<<(x&0xF));
  writeRegister(plane,(y&0x1F0)<<7|0x0404               ,1<<(y&0xF));
  writeRegister(plane,0x0487,0x0000);
}

