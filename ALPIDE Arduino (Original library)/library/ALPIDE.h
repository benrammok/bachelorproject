/***************************************************************************
*  ALPIDE - Arduino reference library for the ALPIDE shield.               *
*  Copyright (C) 2017 Magnus Mager <Magnus.Mager@cern.ch>                  *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/

#ifndef ALPIDE_H
#define ALPIDE_H

#include <Arduino.h>
#include <stdint.h>


// Pinout
// ALPIDE     AVR (Arduino)
// ------     -------------
// PWR_EN     PB1 (9)
// RST_N      PD7 (7) 
// DCLK       PB2 (10)
// CTRL       PB0 (8)
// 1-wire     PD5 (5)

#define ALPIDE_PIN_PWR_EN  7
#define ALPIDE_PIN_RST_N   9
#define ALPIDE_PIN_CLK    11
#define ALPIDE_PIN_CTRL    8
#define ALPIDE_PIN_ONEWIRE 12

#define ALPIDE_REG_COMMAND       0x0000
#define ALPIDE_REG_MODE_CTRL     0x0001
#define ALPIDE_REG_FROMU_CFG1    0x0004
#define ALPIDE_REG_FROMU_CFG2    0x0005
#define ALPIDE_REG_CMUDMU_CFG    0x0010
#define ALPIDE_REG_DMU_FIFO_LO   0x0012
#define ALPIDE_REG_DMU_FIFO_HI   0x0013
#define ALPIDE_REG_DAC_VRESETD   0x0602
#define ALPIDE_REG_DAC_VCASN     0x0604
#define ALPIDE_REG_DAC_VPULSEH   0x0605
#define ALPIDE_REG_DAC_VPULSEL   0x0606
#define ALPIDE_REG_DAC_VCASN2    0x0607
#define ALPIDE_REG_DAC_ITHR      0x060E

#define APIDE_DEFAULT_VRESETD    147 /* TODO: value in mV?*/
//#define APIDE_DEFAULT_VRESETD    200 /* TODO: mV?*/

#define ALPIDE_CMD_TRIGGER       0x0055
#define ALPIDE_CMD_GRST          0x00D2
#define ALPIDE_CMD_PRST          0x00E4
#define ALPIDE_CMD_PULSE         0x0078
#define ALPIDE_CMD_BCRST         0x0036
#define ALPIDE_CMD_RORST         0x0063
#define ALPIDE_CMD_DEBUG         0x00AA
#define ALPIDE_CMD_WROP          0x009C
#define ALPIDE_CMD_RDOP          0x004E
#define ALPIDE_CMD_CMU_CLEAR_ERR 0xFF00
#define ALPIDE_CMD_FIFOTEST      0xFF01
#define ALPIDE_CMD_LOADOBDEFCFG  0xFF02
#define ALPIDE_CMD_XOFF          0xFF10
#define ALPIDE_CMD_XON           0xFF11
#define ALPIDE_MCD_ADCMEASURE    0xFF20

#define ALPIDE_DATA_CHIP_HEADER   0xA0
#define ALPIDE_DATA_CHIP_TRAILER  0xB0
#define ALPIDE_DATA_CHIP_EMPTY    0xE0
#define ALPIDE_DATA_REGION_HEADER 0xC0
#define ALPIDE_DATA_DATA_SHORT    0x40
#define ALPIDE_DATA_BUSY_ON       0xF1
#define ALPIDE_DATA_BUSY_OFF      0xF0

class ALPIDE {
public:
  ALPIDE();
  void    power(bool on=true);
  int     init();
  int     trigger(int strobeMilliSeconds=0);
  int     readRawData(uint8_t plane,uint16_t *buf,int n);
  static int     decodeRawDataWord(uint16_t word,int8_t *currentRegion,uint16_t *x,uint16_t *y,uint8_t *bc,int8_t *plane,uint8_t *flags);
  int     readEvent(uint8_t plane,uint16_t *x,uint16_t *y,int n);
//protected:
  int     broadcastCommand      (uint8_t cmd);
  int     broadcastWriteRegister(uint16_t addr,uint16_t data);
  int     writeRegister         (uint8_t plane,uint16_t addr,uint16_t data);
  int32_t readRegister          (uint8_t plane,uint16_t addr);
  void    maskAllPixels         (uint8_t plane,bool enable);
  void    enablePulserAllPixels (uint8_t plane,bool enable);
  void    maskPixel             (uint8_t plane,uint16_t x,uint16_t y,bool enable);
  void    enablePulserPixel     (uint8_t plane,uint16_t x,uint16_t y,bool enable);
//private:
  void    clk(int n=1);
  int     checkIdle(int n=1);
  void    writeByte(uint8_t byte);
  int16_t readByte();
};

#endif

