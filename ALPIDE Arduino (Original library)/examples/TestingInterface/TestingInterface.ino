#include <ALPIDE.h>

ALPIDE alpide;

int NPLANES = 2;
//unsigned long timenow = 0;
//unsigned long timediff = 0;
void setup() {
  Serial.begin(115200);

  alpide.power();
  alpide.init();

  // USE THESE IF YOU NEED TO FIX SOME VALUES:
  
  //  alpide.writeRegister(0,ALPIDE_REG_DAC_VCASN ,);
    alpide.writeRegister(0,ALPIDE_REG_DAC_VCASN2,45);
  //  alpide.writeRegister(0,ALPIDE_REG_DAC_ITHR  ,);
  
  //  alpide.writeRegister(1,ALPIDE_REG_DAC_VCASN ,);
    alpide.writeRegister(1,ALPIDE_REG_DAC_VCASN2,175);
  //  alpide.writeRegister(3,ALPIDE_REG_FROMU_CFG1,3);
  //  alpide.writeRegister(1,ALPIDE_REG_DAC_ITHR  ,);
  
  //  alpide.writeRegister(2,ALPIDE_REG_DAC_VCASN ,);
  //  alpide.writeRegister(2,ALPIDE_REG_DAC_VCASN2,);
  //  alpide.writeRegister(2,ALPIDE_REG_DAC_ITHR  ,);
/*  
if(alpide.readRegister(3, ALPIDE_REG_DAC_VCASN2) >=0 )
{
  NPLANES = 4;
  
  alpide.maskAllPixels(3,true);
  
  for(int i =299; i< 300;i++)
  {
    for(int j =1; j< 10;j++)
    {
      alpide.maskPixel(3,j,i,false);
    }  
  }
  
}
*/



//alpide.maskPixel(3,79,117,true);
//alpide.maskPixel(3,841,30,true);

}

void trigger() {
  digitalWrite(ALPIDE_PIN_CTRL,LOW);
  pinMode(ALPIDE_PIN_CTRL,OUTPUT);
  asm volatile(
    "  ldi r16,5 ; toggle data and clock                                 \n"
    "  ldi r17,4 ; toggle clock                                          \n"
    "  cli                                                               \n"
    "  out 3,r17         ; clk rise (start): 0                           \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (LSB 0) 1                            \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (1) 0                                \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (2) 1                                \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (3) 0                                \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (4) 1                                \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (5) 0                                \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (6) 1                                \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (MSB 7) 0                            \n"
    "  out 3,r16         ; clk fall                                      \n"
    "  out 3,r17         ; clk rise (stop) 1                             \n"
    "  out 3,r17         ; clk fall                                      \n"
    "  sei                                                               \n"
    :
    :
    :"r16","r17"
  );
}
void loop() {
  alpide.broadcastWriteRegister(ALPIDE_REG_FROMU_CFG2,69); // set strobe length to 69+1
  //alpide.broadcastWriteRegister(ALPIDE_REG_FROMU_CFG2,70); // set strobe length to 70+1
  alpide.trigger(); // trigger, should overlap last strobe and stop right in fron of next strobe
  alpide.broadcastWriteRegister(ALPIDE_REG_FROMU_CFG2,0xFFFF); // set strobe length to max
  Serial.flush();
//  timenow = micros();
 
  alpide.trigger(); // this one here starts the next event and we end the last line right after
  //Serial.println();
  //Serial.write('[');
//   Serial.print("Trigger time:");
//   timediff = micros()-timenow;
//   timenow = micros();
//    Serial.print(timediff);
  bool error=false;
  for (int iplane=0;iplane<NPLANES;++iplane) {
    alpide.trigger(); // long trigger, overlap as often as wanted
    uint16_t x[200];
    uint16_t y[200];
    int nhits=alpide.readEvent(115,x,y,200);
    if (nhits<0) error=true;
    if (iplane) Serial.write('|');
    Serial.print("\{\"nhits\":");
    Serial.print(nhits);
    Serial.print(",\"hits\":[");
    for (int ihit=0;ihit<nhits;++ihit) {
      if (ihit) Serial.write(',');
    //  Serial.write('[');
      Serial.print(x[ihit]);
      Serial.write(',');
      Serial.print(y[ihit]);
    //  Serial.write(']');
    }
    Serial.print("]}");
  }
//     Serial.print("Send time:");
//      timediff = micros()-timenow;
//    Serial.print(timediff);
    Serial.println();

  if (error) {
   // Serial.println("WARNING: readout error occured. Resetting readout.");
    alpide.broadcastCommand(ALPIDE_CMD_RORST);
  }
  //Serial.write(']');
}

