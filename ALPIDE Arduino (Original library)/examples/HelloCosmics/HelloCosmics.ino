/***************************************************************************
*  ALPIDE - Arduino reference library for the ALPIDE shield.               *
*  Copyright (C) 2017 Magnus Mager <Magnus.Mager@cern.ch>                  *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/

#include <ALPIDE.h>

ALPIDE alpide;

#define NPLANES 1

void setup() {
  Serial.begin(9600);

  alpide.power();
  alpide.init();

  // USE THESE IF YOU NEED TO FIX SOME VALUES:
  
  //  alpide.writeRegister(0,ALPIDE_REG_DAC_VCASN ,);
  //  alpide.writeRegister(0,ALPIDE_REG_DAC_VCASN2,);
  //  alpide.writeRegister(0,ALPIDE_REG_DAC_ITHR  ,);
  
  //  alpide.writeRegister(1,ALPIDE_REG_DAC_VCASN ,);
    
    alpide.writeRegister(1,ALPIDE_REG_DAC_VCASN2,175);
  //  alpide.writeRegister(1,ALPIDE_REG_DAC_ITHR  ,);
  
  //  alpide.writeRegister(2,ALPIDE_REG_DAC_VCASN ,);
  //  alpide.writeRegister(2,ALPIDE_REG_DAC_VCASN2,);
  //  alpide.writeRegister(2,ALPIDE_REG_DAC_ITHR  ,);

}

void write_vscan2(uint8_t val)
{
    alpide.writeByte(0x9C);
    alpide.writeByte(115);
    alpide.writeByte(0x07);
    alpide.writeByte(0x06);
    alpide.writeByte(val);
    alpide.writeByte(0x00);
  
}

void loop() {
  Serial.print("Strobing event...");
  alpide.trigger();
  Serial.println(" done.");
  Serial.println("Reading event:");
  bool error=false;
  //for (int plane=0;plane<NPLANES;++plane) {
    //Serial.print("- plane ");
    //Serial.print(plane);
    uint8_t addr=115;
    //alpide.trigger();
    alpide.writeRegister(addr,ALPIDE_REG_DAC_VCASN2,(unsigned int)0x0f);  //0x0607
    //alpide.trigger();
    //write_vscan2(0x0f);
    uint8_t k = 115;
   // for(; k<255; k++)
     {
      Serial.print("Addr - ");
      Serial.print(k);
      Serial.print(" :");
      unsigned int i = alpide.readRegister(k,ALPIDE_REG_DAC_VCASN2);
      Serial.println(i);
      }
   
    //Serial.print(": ");
    uint16_t x[100];
    uint16_t y[100];
    delay(3000);
    //int nhits=alpide.readEvent(0xF3,x,y,100);
    //Serial.println(nhits);
  }
    /*if (nhits<0) error=true;
    Serial.print(nhits);
    Serial.print(" hit(s):");
    for (int ihit=0;ihit<nhits;++ihit) {
      Serial.print(" (");
      Serial.print(x[ihit]);
      Serial.print(",");
      Serial.print(y[ihit]);
      Serial.print(")");
    }
    Serial.println();
  }
  if (error) {
    Serial.println("WARNING: readout error occured. Resetting readout.");
    alpide.broadcastCommand(ALPIDE_CMD_RORST);
  }
  */
  delay(2000);
}

