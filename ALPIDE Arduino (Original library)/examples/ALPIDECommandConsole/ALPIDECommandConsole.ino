/***************************************************************************
*  ALPIDE - Arduino reference library for the ALPIDE shield.               *
*  Copyright (C) 2017 Magnus Mager <Magnus.Mager@cern.ch>                  *
*                                                                          *
*  This program is free software: you can redistribute it and/or modify    *
*  it under the terms of the GNU General Public License as published by    *
*  the Free Software Foundation, either version 3 of the License, or       *
*  (at your option) any later version.                                     *
*                                                                          *
*  This program is distributed in the hope that it will be useful,         *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*  GNU General Public License for more details.                            *
*                                                                          *
*  You should have received a copy of the GNU General Public License       *
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
***************************************************************************/

#include <ALPIDE.h>
#include <OneWire.h>

ALPIDE alpide;
OneWire onewire(ALPIDE_PIN_ONEWIRE);

int readline(char *buf,int n) {
  int i=0;
  int c;
  do {
    while (!Serial.available());
    c=Serial.read();
    if (c=='\b' && i>0) {
      --i;
      Serial.write('\b');
    }
    else if (0x20<=c && c<=0x7E && i<n-1) {
      buf[i++]=c;
      Serial.write(c);
    }
    else if (c=='\r' || c=='\n') {
      Serial.write(c);
    }
    else {
      Serial.write('\a');
    }
  } while (c!='\n');
  buf[i]='\0';
}

void read_IDs() {
  byte addr[8];
  onewire.reset_search();
  int n=0;
  while (onewire.search(addr)) {
    Serial.print("-> Found plane at address:");
    for (int i=0;i<8;++i) {
      Serial.write(' ');
      Serial.print(addr[i],HEX);
    }
    if (OneWire::crc8(addr,7)!=addr[7]) {
      Serial.println(" - ERROR: invalid CRC!");
    }
    else if (addr[0]!=0x28) {
      Serial.println(" - ERROR: not supported 1-wire device on bus");
    } 
    else {
      onewire.reset();
      onewire.select(addr);
      onewire.write(0x44,1);
      delay(1000);
      onewire.reset();
      onewire.select(addr);    
      onewire.write(0xBE);
      byte data[2];
      for (int i=0;i<2;++i) {
        data[i]=onewire.read();
      }
      int16_t raw=(data[1]<<8)|data[0];
      Serial.print(" - T = ");
      Serial.print((float)raw/16.0);
      Serial.println(" C");
      ++n;
    }
  }
  Serial.print(F("-> Found total of "));
  Serial.print(n);
  Serial.println(F(" plane(s)."));
} 

void trigger(char *args) {
  int nus=strtol(args,0,0);
  Serial.print(F("-> Triggering with strobe time of "));
  Serial.print(nus);
  Serial.println(F(" milliseconds."));
  alpide.trigger(nus);
}

void print_cluster(uint32_t *hits,int nhits,int icl) {
  uint16_t xmin=1024;
  uint16_t xmax=   0;
  uint16_t ymin= 512;
  uint16_t ymax=   0;
  int clsize=0;
  for (int i=0;i<nhits;++i) {
    int ii=hits[i]>>24&0xFF;
    if (ii==icl) {
      uint16_t xi=hits[i]>>9&0x3FF;
      uint16_t yi=hits[i]>>0&0x1FF;
      if (xi<xmin) xmin=xi;
      if (xi>xmax) xmax=xi;
      if (yi<ymin) ymin=yi;
      if (yi>ymax) ymax=yi;
      ++clsize;
    }
  }
  if (clsize<1) return;
  if (clsize==1) {
    Serial.print("1-pixel cluster at x=");
    Serial.print(xmin);
    Serial.print(", y=");
    Serial.println(ymin);
    return;
  }
  Serial.print(xmax-xmin+1); 
  Serial.print("x"); 
  Serial.print(ymax-ymin+1); 
  Serial.print("-pixel cluster with total of "); 
  Serial.print(clsize); 
  Serial.println(" hits: "); 
  Serial.print("    ");
  Serial.write('.');
  for (int i=0;i<xmax-xmin+1;++i) {
    Serial.write('-');
  }
  Serial.println(".");
  for (int y=ymin;y<=ymax;++y) {
    if (y==ymin || y==ymax) {
      if (y>=1000)
        Serial.write('1');
      else
        Serial.write(' ');
      if (y>=100)
        Serial.write('0'+y/100%10);
      else
        Serial.write(' ');
      if (y>=10)
        Serial.write('0'+y/10%10);
      else
        Serial.write(' ');
      Serial.write('0'+y%10);
      Serial.write('+');
    }
    else {
      Serial.print("    |");
    }
    for (int x=xmin;x<=xmax;++x) {
      bool hit=false;
      for (int i=0;i<nhits;++i) {
        int ii=hits[i]>>24&0xFF;
        if (ii==icl) {
          int xi=hits[i]>>9&0x3FF;
          int yi=hits[i]>>0&0x1FF;
          if (xi==x && yi==y) {
            hit=true;
            break;
          }
        }
      }
      if (hit)
         Serial.write('#');
      else
         Serial.write(' ');
    }
    Serial.println("|");
  }
  Serial.print("    '+");
  for (int x=xmin+1;x<=xmax-1;++x) {
    Serial.write('-');
  }
  if (xmin!=xmax) {
    Serial.print("+");
  } 
  Serial.println("'");

  Serial.print("     ");
  Serial.write('0'+xmin%10);
  for (int i=1;i<xmax-xmin;++i) {
      Serial.write(' ');
  }
  if (xmin!=xmax) {
    Serial.write('0'+xmax%10);
  }
  Serial.println();

  Serial.print("     ");
  if (xmin>=10)
    Serial.write('0'+xmin/10%10);
  else
    Serial.write(' ');
  for (int i=1;i<xmax-xmin;++i) {
      Serial.write(' ');
  }
  if (xmin!=xmax) {
    if (xmax>=10)
      Serial.write('0'+xmax/10%10);
    else
      Serial.write(' ');
  }
  Serial.println();

  Serial.print("     ");
  if (xmin>=100)
    Serial.write('0'+xmin/100%10);
  else
    Serial.write(' ');
  for (int i=1;i<xmax-xmin;++i) {
      Serial.write(' ');
  }
  if (xmin!=xmax) {
    if (xmax>=100)
      Serial.write('0'+xmax/100%10);
    else
      Serial.write(' ');
  }
  Serial.println();

  Serial.print("     ");
  if (xmin>=1000)
    Serial.write('1');
  else
    Serial.write(' ');
  for (int i=1;i<xmax-xmin;++i) {
      Serial.write(' ');
  }
  if (xmin!=xmax) {
    if (xmax>=1000)
      Serial.write('1');
    else
      Serial.write(' ');
  }
  Serial.println();

}

int find_clusters(uint32_t *hits,int nhits) {
  // hits are originally saved as x<<9|y
  // we use the same for clusters by adding a cluster icl as in: icl<<24|x<<9|y
  // 1 SORT THE HITS ACCORDING TO X, Y (LEXIOGRAPHICALLY)
  for (int i=0;i<nhits;++i)
    for (int j=i+1;j<nhits;++j)
      if (hits[j]<hits[i]) {
        uint32_t tmp=hits[i];
        hits[i]=hits[j];
        hits[j]=tmp;
      }
  int ncl=0;
  // 2 FOR EACH HIT CHECK IF A PREVIOUS HIT IS ADJACENT TO IT
  for (int i=0;i<nhits;++i) {
    int xi=hits[i]>>9&0x3FF;
    int yi=hits[i]>>0&0x1FF;
    int ii=-1;
    for (int j=0;j<i;++j) {
      int xj=hits[j]>>9&0x3FF;
      int yj=hits[j]>>0&0x1FF;
      int ij=hits[j]>>24&0xFF;
      if (yi==yj && xi-xj==1 || xi==xj && yi-yj==1) {
        if (ii==-1) {
          ii=hits[j]>>24&0xFF;
          hits[i]=hits[i]&0x00FFFFFF|(uint32_t)ii<<24;
        } 
        else if (ii!=ij) {
          for (int k=0;k<i;++k) {
            int ik=hits[k]>>24&0xFF;
            if (ik==ij)
              hits[k]=hits[k]&0x00FFFFFF|(uint32_t)ii<<24;
          }
          --ncl;
        }
      } 
    }
    if (ii==-1) {
      hits[i]=hits[i]&0x00FFFFFF|(uint32_t)ncl<<24;
      ++ncl;
    }
  }
  return ncl;
}

void analyse_clusters(uint32_t *hits,int nhits) {
  int ncl=find_clusters(hits,nhits);
  Serial.print(F("Found "));
  Serial.print(ncl);
  Serial.println(F(" clusters:"));
  for (int icl=0;icl<ncl;++icl) {
    Serial.print(F(" - "));
    print_cluster(hits,nhits,icl);
  }
}

void threshold_scan(char *args) {
  int plane=strtol(args,&args,0);
  int x    =strtol(args,&args,0);
  int y    =strtol(args,&args,0);
  int cmin =strtol(args,&args,0);
  int cmax =strtol(args,&args,0);
  int n    =strtol(args,0    ,0);
  if (cmax==0) cmax=30;
  if (n   ==0) n   =10;
  Serial.print(F("-> Doing threshold scan for plane "));
  Serial.print(plane);
  Serial.print(F(", pixel x="));
  Serial.print(x);
  Serial.print(F(", y="));
  Serial.print(y);
  Serial.print(F(", charge range=["));
  Serial.print(cmin);
  Serial.print(':');
  Serial.print(cmax);
  Serial.print(F("], "));
  Serial.print(n);
  Serial.println(F(" events/charge"));
  alpide.maskAllPixels        (plane,true );
  alpide.enablePulserAllPixels(plane,false);
  alpide.maskPixel            (plane,x,y,false);
  alpide.enablePulserPixel    (plane,x,y,true );
  alpide.writeRegister(plane,ALPIDE_REG_FROMU_CFG1,0x0060);
  alpide.writeRegister(plane,8,10);
  alpide.writeRegister(plane,5,8);
  for (int c=cmin;c<=cmax;++c) {
    alpide.writeRegister(plane,ALPIDE_REG_DAC_VPULSEH,170);
    alpide.writeRegister(plane,ALPIDE_REG_DAC_VPULSEL,170-c);
    Serial.print(F("  - charge "));
    Serial.print(c);
    Serial.print(F("\t: "));
    int nhit=0;
    for (int i=0;i<n;++i) {
//      alpide.writeRegister(plane,ALPIDE_REG_COMMAND,ALPIDE_CMD_PULSE);
      asm volatile(
        "ldi r16,5 ; toggle data and clock \n"
        "ldi r17,4 ; toggle clock          \n"
        "cbi 5,2   ; init clock (low)      \n"
        "cbi 5,0   ; start bit (low)  0    \n"
        "out 3,r17 ; clk rise (start) 0    \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise (0 LSB) 0    \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise (1)     0    \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise (2)     0    \n"
        "out 3,r16 ; clk fall,toggle  x    \n"
        "out 3,r17 ; clk rise (3)     1    \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise (4)     1    \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise (5)     1    \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise (6)     1    \n"
        "out 3,r16 ; clk fall,toggle  x    \n"
        "out 3,r17 ; clk rise (7 MSB) 0    \n"
        "out 3,r16 ; clk fall,toggle  x    \n"
        "out 3,r17 ; clk rise (stop)  1    \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
        "out 3,r17 ; clk rise              \n"
        "out 3,r17 ; clk fall              \n"
       :::"r16","r17"
      );
      uint16_t xread[10];
      uint16_t yread[10];
      int ret=alpide.readEvent(plane,xread,yread,10);
      if (ret<0) {
        Serial.print(F("ERROR reading event: "));
        Serial.println(ret);
        return;
      }
      bool hit=false;
      for (int j=0;j<ret;++j) {
        if (xread[j]==x && yread[j]==y) hit=true;
      }
      Serial.print(hit?'x':'_');
      if (hit) ++nhit;
    } 
    Serial.print(F(" : "));
    Serial.print(nhit);
    Serial.print('/');
    Serial.println(n);
  } 
}

void read_plane(char *args) {
  int plane=strtol(args,0,0);
  Serial.print("-> Reading data from plane ");
  Serial.print(plane);
  Serial.println(":");
  uint32_t n=0;
  int ret;
  int8_t currentRegion=-1;
  uint32_t hits[128];
  int nhits=0;
  do {
    uint16_t data;
    ret=alpide.readRawData(plane,&data,1);
    if (ret==1) {
      Serial.print("-> ");
      Serial.print(data,HEX);
      Serial.print('\t');
      uint16_t x;
      uint16_t y;
      uint8_t  bc;
      int8_t   plane;
      uint8_t  flags;
      int type=ALPIDE::decodeRawDataWord(data,&currentRegion,&x,&y,&bc,&plane,&flags);
      switch (type) {
        case ALPIDE_DATA_CHIP_HEADER:
          Serial.print("CHIP HEADER: plane=");
          Serial.print(plane,DEC);
          Serial.print(", bunch crossing=");
          Serial.println(bc,DEC);
          nhits=0;
          break;
        case ALPIDE_DATA_CHIP_TRAILER:
          Serial.print("CHIP TRAILER: flags=");
          Serial.println(flags,HEX);
          analyse_clusters(hits,nhits);
          break;
        case ALPIDE_DATA_CHIP_EMPTY:
          Serial.print("CHIP EMPTY EVENT: plane=");
          Serial.print(plane,DEC);
          Serial.print(", bunch crossing=");
          Serial.println(bc,DEC);
          break;
        case ALPIDE_DATA_REGION_HEADER:
          Serial.print("REGION HEADER: region=");
          Serial.println(currentRegion);
          break;
        case ALPIDE_DATA_DATA_SHORT:
          Serial.print("DATA SHORT: x=");
          Serial.print(x);
          Serial.print(", y=");
          Serial.println(y);
          if (nhits<128) hits[nhits++]=(uint32_t)x<<9|y;
          break;
        case ALPIDE_DATA_BUSY_ON:
          Serial.println("BUSY ON");
          break;
        case ALPIDE_DATA_BUSY_OFF:
          Serial.println("BUSY OFF");
          break;
        default:
          Serial.print("decoding error ");
          Serial.println(type,DEC);
          break;
      }
      ++n;
    }
  } while (ret==1);
  if (ret<0) {
    Serial.print("-> Error ");
    Serial.println(ret);
  }
  Serial.print("-> Read total of ");
  Serial.print(n);
  Serial.println(" words.");
}

void read_register(char *args) {
  int plane=strtol(args,&args,0);
  int addr =strtol(args,0    ,0);
  Serial.print("-> Reading register 0x");
  Serial.print(addr,HEX);
  Serial.print(" from plane ");
  Serial.print(plane);
  Serial.print(": ");
  int32_t ret=alpide.readRegister(plane,addr);
  if (ret>=0) {
    Serial.print("0x");
    Serial.print(ret,HEX);
    Serial.print(" (or ");
    Serial.print(ret);
    Serial.println(" in decimal)");
  }
  else {
    Serial.print("error ");
    Serial.println(ret);
  }
}

void write_register(char *args) {
  int plane=strtol(args,&args,0);
  int addr =strtol(args,&args,0);
  int data =strtol(args,0    ,0);
  Serial.print("-> Writing 0x");
  Serial.print(data,HEX);
  Serial.print(" to register 0x");
  Serial.print(addr,HEX);
  Serial.print(" of plane ");
  Serial.print(plane);
  Serial.println(".");
  alpide.writeRegister(plane,addr,data);
}

void broadcast_command(char *args) {
  int cmd=strtol(args,0,0);
  Serial.print("-> Broadcasting command 0x");
  Serial.print(cmd,HEX);
  Serial.println(".");
  alpide.broadcastCommand(cmd);
}

void usage() {
  Serial.println(F(
    "ALPIDE terminal\n"
    "===============\n"
    "\n"
    "USAGE:\n"
    "  P                      : power on\n"
    "  p                      : power off\n"
    "  i                      : initialise\n"
    "  t strobe-milli-seconds : trigger\n"
    "  o plane                : read out\n"
    "  r plane address        : read register\n"
    "  w plane address data   : write register\n"
    "  c command              : broadcast command\n"
    "  T plane x y min-charge max-charge events-per-charge : threshold scan\n"
    "  I                      : read plane ID(s)+temperature(s)\n"
    "EXAMPLES:\n"
    "  power chip and initialise:\n"
    "    P\n"
    "    i\n"
    "  read strobe counter register of plane 0:\n"
    "    r 0 10\n"
    "  write ITHR DAC of plane 1 to 30\n"
    "    w 1 0x060E 30\n"
    "  trigger and read out event of plane 0 and 1\n"
    "    t 1\n"
    "    o 0\n"
    "    o 1\n"
  ));
}

void setup() {
  Serial.begin(9600);
  Serial.println();
  usage();
}

void loop() {
  Serial.println();
  Serial.print("INPUT (u=usage) > ");
  char line[81];
  int n=readline(line,sizeof(line));
  int i=0;
  while (i<n && isspace(line[i])) ++i;
  switch (line[i]) { 
    case 'P':
      Serial.println("-> Powering on.");
      alpide.power(true);
      break;
    case 'p':
      Serial.println("-> Powering off.");
      alpide.power(false);
      break;
    case 'i':
      Serial.println("-> Initialising.");
      alpide.init();
      break;
    case 'o':
      read_plane(&line[i+1]);
      break;
    case 't':
      trigger(&line[i+1]);
      break;
    case 'r':
      read_register(&line[i+1]);
      break;
    case 'w':
      write_register(&line[i+1]);
      break;
    case 'c':
      broadcast_command(&line[i+1]);
      break;
    case 'T':
      threshold_scan(&line[i+1]);
      break;
    case 'I':
      read_IDs();
      break;
    case 'u':
      usage();
      break;
    default:
      Serial.println("-> Bad input...");
      usage();
      break;
  }
}

